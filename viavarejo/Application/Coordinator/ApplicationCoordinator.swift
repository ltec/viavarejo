//
//  ApplicationCoordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ApplicationCoordinator: Coordinator {
    weak var parentCoordinator: Coordinator?
    
    let window: UIWindow
    private let navigationController: UINavigationController
    var childrenCoordinator: [String : Coordinator]?
    
    init(window: UIWindow) {
        self.window = window
        navigationController = UINavigationController()
        navigationController.navigationBar.tintColor = PalleteColor.primary
        navigationController.navigationBar.barTintColor = PalleteColor.secondary
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : PalleteColor.primary]
    }
    
    func start() {
        window.rootViewController = navigationController
        let mainCoordinator = MainCoordinator(navigationController: navigationController)
        mainCoordinator.start()
        setChildrenCoordinator(coordinator: mainCoordinator)
        window.makeKeyAndVisible()
    }
}
