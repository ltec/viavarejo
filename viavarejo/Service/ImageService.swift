//
//  ImageService.swift
//  viaverejo
//
//  Created by Ti Corporativo on 17/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol ImageServiceProtocol {
    func downloadImage(from url: String, callback: @escaping (UIImage?) -> Void)
}

class ImageService: ImageServiceProtocol {
    func downloadImage(from url: String, callback: @escaping (UIImage?) -> Void) {
        guard let urlRequest = URL(string: url) else { return }
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    callback(nil)
                    return
            }
            DispatchQueue.main.async() {
                callback(image)
            }
            }.resume()
    }
}
