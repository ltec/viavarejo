//
//  AlertService.swift
//  viavarejoTests
//
//  Created by Ti Corporativo on 17/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//
import UIKit

protocol AlertServiceProtocol: class {
    func start(controller: UIViewController)
    func showAlertListView(title: String, message: String?, items: [String])
    func showAlertLoading(completion: (() -> Void)?)
    func hideAlertLoading()
}

class AlertService: AlertServiceProtocol {
    
    var controller: UIViewController?
    private lazy var alertView = AlertView(title: "", message: "", preferredStyle: .alert)
    
    func start(controller: UIViewController) {
        self.controller = controller
    }
    
    func showAlertListView(title: String, message: String?, items: [String]){
        guard let controller = self.controller else { return }
        
        self.alertView.title = title
        self.alertView.message = message
        if let instance = self as? AlertViewDelegate {
            alertView.flowDelegate = instance
        }
        alertView.loadItems(items: items)
        controller.present(alertView, animated: true, completion: nil)
    }
    
    func showAlertLoading(completion: (() -> Void)? = nil){
        
        guard let controller = self.controller else { return }

        self.alertView.title = "Carregando..."
        self.alertView.message = nil
        
        alertView.view.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        alertView.view.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: alertView.view.centerXAnchor, constant: 0).isActive = true
        let centerYConstraint = NSLayoutConstraint(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: alertView.view, attribute: .centerY, multiplier: 1.4, constant: 0)
        NSLayoutConstraint.activate([centerYConstraint])
        
        indicator.isUserInteractionEnabled = false
        indicator.startAnimating()
        
        controller.present(alertView, animated: true, completion: {
            if let callback = completion {
                callback()
            }
        })
    }
    
    func hideAlertLoading(){
        guard self.controller != nil else { return }
        self.alertView.dismiss(animated: true, completion: nil)
    }
}
