//
//  CalculeValueZipCodeView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class CalculeValueZipCodeView : UIView {
    
    
    @IBOutlet weak var buttonView: InfoActionButtonView!
    
    @IBOutlet weak var textFieldView: InfoActionTextFieldView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        buttonView.configContentView(backgroundColor: PalleteColor.blueAlice)
        
        buttonView.setupActionButton(text: "Comprar", color: PalleteColor.greenLima, type: .Primary, size: .Large)
        
        textFieldView.setupTitleLabel(text: "CALCULE O FRETE", font: UIFont.headlineThree(), color: PalleteColor.bluePrimary)
        
        textFieldView.setupIconTitleButton(image: UIImage(named: "question"), tintColor: PalleteColor.greyPrimary, isHidden: false)
        
        textFieldView.setupIconActionButton(image: UIImage(named: "location"), tintColor: UIColor.white, backgroundColor: PalleteColor.bluePrimary)
        
        textFieldView.setPlaceholder(placeholder: "Digite seu CEP")
    }
}
