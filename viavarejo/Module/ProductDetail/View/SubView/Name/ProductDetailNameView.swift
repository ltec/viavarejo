//
//  ProductDetailNameView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductDetailNameView: UIView {
   
    @IBOutlet weak var nameLabel: ProductNameLabel!
    @IBOutlet weak var codeItemLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupStyle()
    }
    
    func setupStyle(){
        codeItemLabel.font = UIFont.captionRegular()
        codeItemLabel.textColor = PalleteColor.greyPrimary
    }
    
    func bind(name: String, codeItem: Int){
        nameLabel.text = name
        codeItemLabel.text = "Cód. Item \(String(codeItem))"
    }
    
}
