//
//  EvaluationTableView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class EvaluationTableView : UITableView {
    
    var items: [OpinionDTO] = [] {
        didSet{
            self.reloadData()
        }
    }
    
    private let identifier = "EvaluationTableViewCell"
    private var height: NSLayoutConstraint?
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        self.height = self.heightAnchor.constraint(equalToConstant: 0.5)
        self.height?.isActive = true
        self.separatorColor = PalleteColor.greyPrimary
        self.isScrollEnabled = false
        self.dataSource = self
        self.delegate = self
    }
}

extension EvaluationTableView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! EvaluationTableViewCell
        let item = self.items[indexPath.row]
        cell.bind(opinion: item)
        return cell
    }
}

extension EvaluationTableView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.height?.constant = (self.height?.constant ?? 0) + cell.frame.height
    }
}
