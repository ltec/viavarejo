//
//  EvaluationContentView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class EvaluationContentView : UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var headerView: EvaluationHeaderView!
    
    @IBOutlet weak var tableView: EvaluationTableView!
    
    @IBOutlet weak var viewActionButton: ButtonView!
    
    @IBOutlet weak var stateView: StateFullView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        stateView.state = .loading("Carregando...")
        styleLabel()
        stylebutton()
    }
    
    func styleLabel(){
        titleLabel.setupGroupTitle()
        titleLabel.text = "AVALIAÇÕES E OPINIÕES"
    }
    
    func stylebutton(){
        viewActionButton.type = .Clear
        viewActionButton.size = .Small
        viewActionButton.contentHorizontalAlignment = .trailing
        viewActionButton.referenceColor = PalleteColor.bluePrimary
        viewActionButton.setButtonTitle("VER TODAS AVALIAÇÕES")
    }
    
    func bind(evaluation: EvaluationDTO){
        
        headerView.evaluationValueView.titleLabel.text = String(evaluation.evaluation)
        headerView.evaluationValueView.subTitleLabel.text = "5"
        
        headerView.evaluationView.evaluation = Int(evaluation.evaluation)
        
        headerView.evaluationCountLabel.text = "\(String(evaluation.qtd)) avaliações"
        
        tableView.items = evaluation.opinions
        
        stateView.state = .defaultLayout
    }
    
}
