//
//  EvaluationTableViewCell.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class EvaluationTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var twoLabelView: TwoLabelView!
    
    @IBOutlet weak var evaluationView: EvaluationView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLabel()
        
    }
    
    func setupLabel(){
        twoLabelView.setupTitle(text: "Label1", font: UIFont.captionBold(), color: UIColor.black)
        twoLabelView.setupSubTitle(text: "Label2", font: UIFont.captionRegular(), color: PalleteColor.greyPrimary)
    }
    
    func bind(opinion: OpinionDTO){
        self.evaluationView.evaluation = opinion.note
        
        let date = Date()
        if let evaluationDate = date.dateFrom(string: opinion.date, withFormat: Date.yyyyMMddHHmmssFormat) {
            self.twoLabelView.titleLabel.text = "\(evaluationDate.asString(withFormat: Date.ddMMyyyBRFormat)) - \(opinion.client)"
        }
    
        self.twoLabelView.subTitleLabel.text = opinion.comment
    }
    
}
