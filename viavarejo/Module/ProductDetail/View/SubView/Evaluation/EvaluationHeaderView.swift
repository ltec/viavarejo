//
//  ProductDetailEvaluatioView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class EvaluationHeaderView : UIView {
    
    @IBOutlet weak var evaluationValueView: EvaluationValueView!
    @IBOutlet weak var evaluationView: EvaluationView!
    @IBOutlet weak var evaluationCountLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        styleLabel()
        
        evaluationView.evaluation = 4
    }
    
    func styleLabel(){
        
        evaluationCountLabel.setup(font: UIFont.bodyRegular(), color: PalleteColor.greyPrimary)
    }
}
