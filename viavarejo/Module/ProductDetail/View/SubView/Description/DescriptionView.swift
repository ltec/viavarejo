//
//  DescriptionView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class DescriptionView: UIView {
    
    @IBOutlet weak var twoLabelView: TwoLabelView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        twoLabelView.setupTitle(text: "DESCRIÇÃO", font: UIFont.headlineThree(), color: PalleteColor.bluePrimary)
        
        twoLabelView.setupSubTitle(text: "Label", font: UIFont.captionBold(), color: PalleteColor.greyPrimary)
    }
    
    func bind(description: String){
        twoLabelView.subTitleLabel.text = description
    }
}
