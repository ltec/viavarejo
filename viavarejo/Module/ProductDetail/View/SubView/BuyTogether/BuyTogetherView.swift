//
//  BuyTogetherView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class BuyTogetherView : UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var iconButton: UIButton!
    
    @IBOutlet weak var identifieTwoLabelView: TwoLabelView!
    @IBOutlet weak var infoTwoLabelView: TwoLabelView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupStyle()
    }
    
    func bind(name: String, price: String){
        identifieTwoLabelView.titleLabel.text = name
        identifieTwoLabelView.subTitleLabel.text = price
        
        infoTwoLabelView.titleLabel.text = "Você receberá as informações de agendamento por e-mail"
        infoTwoLabelView.subTitleLabel.text = "Confira mais detalhes sobre o serviço"
    }
    
    func setupStyle(){
        styleLabel()
        styleContentView()
        styleIconButton()
        styleTwoLabelView()
    }
    
    func styleLabel(){
        titleLabel.setup(font: UIFont.bodyRegular(), color: PalleteColor.greyPrimary)
        titleLabel.text = "Aproveite e contrate também:"
    }
    
    func styleContentView(){
        contentView.backgroundColor = PalleteColor.greenSecondary
        contentView.layer.cornerRadius = 2
        contentView.clipsToBounds = true
    }
    
    func styleIconButton(){
        iconButton.layer.borderColor = UIColor.black.cgColor
        iconButton.layer.borderWidth = 1
        iconButton.layer.cornerRadius = 3
        iconButton.clipsToBounds = true
        
        iconButton.setImage(UIImage(named: "tv")?.withRenderingMode(.alwaysTemplate), for: .normal)
        iconButton.tintColor = UIColor.black
        iconButton.backgroundColor = UIColor.white
        iconButton.imageView?.contentMode = .scaleToFill
    }
    
    func styleTwoLabelView(){
        identifieTwoLabelView.setupTitle(text: "Label", font: UIFont.bodyBold(), color: UIColor.black)
        identifieTwoLabelView.setupSubTitle(text: "Label", font: UIFont.captionRegular(), color: PalleteColor.greyPrimary)
        
        infoTwoLabelView.setupTitle(text: "Label", font: UIFont.captionRegular(), color: PalleteColor.greyPrimary)
        infoTwoLabelView.setupSubTitle(text: "Label", font: UIFont.overlineBold(), color: PalleteColor.bluePrimary)
    }
}
