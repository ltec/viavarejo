//
//  ResponsibleView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ResponsibleView: UIView {
    
    @IBOutlet weak var buttonView: InfoActionButtonView!
    
    @IBOutlet weak var textFieldView: InfoActionTextFieldView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        buttonView.setupTitleLabel(text: "VENDIDO E ENTREGUE POR", font: UIFont.headlineThree(), color: PalleteColor.bluePrimary, isHidden: false)
    
        
        buttonView.setupActionButton(text: "", color: PalleteColor.bluePrimary, type: .Secondary, size: .Medium)
        
        textFieldView.setupTitleLabel(text: "Também vendido por:", font: UIFont.captionRegular(), color: PalleteColor.greyPrimary)
        
        textFieldView.setupIconActionButton(image: UIImage(named: "supermarket_car"), tintColor: UIColor.white, backgroundColor: PalleteColor.greenLima)
        
        textFieldView.setupActionButton(text: "Ver todas as lojas", color: PalleteColor.bluePrimary, isHidden: false)
    }
    
    func bind(name: String, price: Double){
        buttonView.actionButton.setButtonTitle("\(name) - \(price.parseCurrency())")
    }
}
