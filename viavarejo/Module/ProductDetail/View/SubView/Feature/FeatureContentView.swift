//
//  FeatureContentView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

enum FeatureEnum : Int {
    case Feature = 0
    case Specification = 1
    
    var getRawValue: Int {
        return self.rawValue
    }
}

class FeatureContentView : UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: FeatureTableView!
    var type: FeatureEnum = .Feature
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        titleLabel.setupGroupTitle()
    }
    
    func bind(infos: [InfoDTO]){
        let info = infos[type.getRawValue]
        self.titleLabel.text = info.description.uppercased()
        self.tableView.items = info.values
    }
}
