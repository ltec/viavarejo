//
//  FeatureTableViewCell.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class FeatureTableViewCell: UITableViewCell {
    
    @IBOutlet weak var twoLabelView: TwoLabelView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup(){
        
        twoLabelView.setupTitle(text: "Label1", font: UIFont.captionBold(), color: UIColor.black)
        
        twoLabelView.setupSubTitle(text: "Label2", font: UIFont.captionRegular(), color: PalleteColor.greyPrimary)
    }

    func bind(title: String, subTitle: String){
        twoLabelView.titleLabel.text = title
        twoLabelView.subTitleLabel.text = subTitle
    }
    
}
