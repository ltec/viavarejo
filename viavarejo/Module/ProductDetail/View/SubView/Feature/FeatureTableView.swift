//
//  FeatureTableView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class FeatureTableView : UITableView {
    
    private let identifier = "FeatureTableViewCell"
    private var height: NSLayoutConstraint?
    
    var items: [ValueDTO] = [] {
        didSet {
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        self.height = self.heightAnchor.constraint(equalToConstant: 0.5)
        self.height?.isActive = true
        self.separatorStyle = .none
        self.isScrollEnabled = false
        self.dataSource = self
        self.delegate = self
    }
}

extension FeatureTableView : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FeatureTableViewCell
        let item = self.items[indexPath.row]
        cell.bind(title: item.name, subTitle: item.value)
        return cell
    }
}

extension FeatureTableView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.height?.constant = (self.height?.constant ?? 0) + cell.frame.height
    }
}
