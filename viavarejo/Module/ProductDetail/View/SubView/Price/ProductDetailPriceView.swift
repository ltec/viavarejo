//
//  ProductDetailPriceView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductDetailPriceView : UIView {
    
    @IBOutlet weak var priceView: PriceView!
    
    @IBOutlet weak var viewParcelLabel: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupStyle()
    }
    
    func setupStyle(){
        priceView.discountLabel.isHidden = false
        viewParcelLabel.font = UIFont.captionRegular()
        viewParcelLabel.textColor = PalleteColor.bluePrimary
        viewParcelLabel.text = "VER PARCELAS"
    }
    
    func bind(price: ProductDetailPriceDTO){
        self.priceView.oldPriceLabel.text = price.oldPrice.parseCurrency()
        self.priceView.priceLabel.text = price.price.parseCurrency()
        self.priceView.parcelLabel.text = price.paymentPlain
        self.priceView.discountLabel.text = "\(String(price.discountPercentage))%"
    }
}
