//
//  ProductDetailCoordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 13/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductDetailCoordinator: Coordinator {
    weak var parentCoordinator: Coordinator?
    
    private let navigationController: UINavigationController
    private var productDetailViewController: ProductDetailViewController?
    var childrenCoordinator: [String : Coordinator]?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let productDetailViewController = ProductDetailViewController()
        productDetailViewController.title = "Detalhe do Produto"
        
        let baritem = UIBarButtonItem(image: UIImage(named: "favorite"), style: .plain, target: nil, action: nil)
        
        productDetailViewController.navigationItem.rightBarButtonItem = baritem
        
        productDetailViewController.coordinator = self
        self.navigationController.pushViewController(productDetailViewController, animated: true)
        self.productDetailViewController = productDetailViewController
    }
}
