//
//  ProductDetailViewController.swift
//  viaverejo
//
//  Created by Ti Corporativo on 13/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductDetailViewController: BaseViewController {
    
    @IBOutlet weak var stateView: StateViewController!
    
    @IBOutlet weak var carouselView: CarouselContentView!
    
    @IBOutlet weak var nameView: ProductDetailNameView!
    
    @IBOutlet weak var priceView: ProductDetailPriceView!
    @IBOutlet weak var buyTogetherView: BuyTogetherView!
    
    @IBOutlet weak var zipCodeView: CalculeValueZipCodeView!
    
    @IBOutlet weak var responsibleView: ResponsibleView!
    
    @IBOutlet weak var descriptionView: DescriptionView!
    
    @IBOutlet weak var featureView: FeatureContentView!
    
    @IBOutlet weak var evaluationView: EvaluationContentView!
    
    @IBOutlet weak var buyTogetherContentView: ProductCollectionContentView!
    
    @IBOutlet weak var sawBoughtContentView: ProductCollectionContentView!
    
    @IBOutlet weak var alsoSawContentView: ProductCollectionContentView!
    
    @IBOutlet weak var specificationView: FeatureContentView!
    
    var productDetailService: ProductDetailServiceProtocol!
    var alertService: AlertServiceProtocol!
    
    init(
        productDetailService: ProductDetailServiceProtocol = ProductDetailService(),
        alertService: AlertServiceProtocol = AlertService()
        ){
        super.init(nibName: nil, bundle: nil)
        
        self.productDetailService = productDetailService
        alertService.start(controller: self)
        self.alertService = alertService
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.alertService.showAlertLoading(completion: {
            self.getProduct()
            self.getEvaluations()
            self.getBuyTogether()
            self.getAlsoSaw()
            self.getSawBought()
        })
        setup()
    }
    
    func setup(){
        self.carouselView.typeLoadImage = .URL
        self.specificationView.type = .Specification
    }
    
    func getProduct(){
        self.productDetailService.getProduct(callback: {(result, error) in
            if let item = result {
                
                self.nameView.bind(name: item.name, codeItem: item.id)
                self.descriptionView.bind(description: item.description)
                self.priceView.bind(price: item.model.defaultt.price)
                self.buyTogetherView.bind(name: item.model.defaultt.services[0].name, price: item.model.defaultt.services[0].price)
                self.carouselView.items = item.model.defaultt.images.map{$0.url.replacingOccurrences(of: "http", with: "https")}
                
                let logist = item.model.defaultt.marketplace.logistDefault
                
                self.responsibleView.bind(name: logist.name, price: logist.price)
                
                self.featureView.bind(infos: item.info)
                self.specificationView.bind(infos: item.info)
                self.stateView.state = .defaultLayout
                self.alertService.hideAlertLoading()
            }
        })
    }
    
    func getEvaluations(){
        self.productDetailService.getEvaluations(callback: {(result, error) in
            if let item = result {
                self.evaluationView.bind(evaluation: item)
            }
        })
    }
    
    func getBuyTogether(){
        self.productDetailService.getBuyTogether(callback: {(result, error) in
            if let items = result {
                self.buyTogetherContentView.bind(items: items)
            }else {
                self.buyTogetherContentView.stateError()
            }
        })
    }
    
    func getSawBought(){
        self.productDetailService.getSawBought(callback: {(result, error) in
            if let items = result {
                self.sawBoughtContentView.bind(items: items)
            }else {
                self.sawBoughtContentView.stateError()
            }
        })
    }
    
    func getAlsoSaw(){
        self.productDetailService.getSawBought(callback: {(result, error) in
            if let items = result {
                self.alsoSawContentView.bind(items: items)
            }else{
                self.alsoSawContentView.stateError()
            }
        })
    }

}
