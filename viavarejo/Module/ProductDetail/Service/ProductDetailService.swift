//
//  ProductDetailService.swift
//  viaverejo
//
//  Created by Ti Corporativo on 17/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

protocol ProductDetailServiceProtocol {
    func getProduct(callback:  @escaping callback<ProductDetailDTO>)
    func getEvaluations(callback:  @escaping callback<EvaluationDTO>)
    func getBuyTogether(callback:  @escaping callback<[ProductItemDTO]>)
    func getSawBought(callback:  @escaping callback<[ProductItemDTO]>)
    func getgetAlsoSaw(callback:  @escaping callback<[ProductItemDTO]>)
}

class ProductDetailService : ProductDetailServiceProtocol {
    func getEvaluations(callback: @escaping callback<EvaluationDTO>) {
        ThreadRequest.execute(threadParams: RequestBuilder<EvaluationDTO>()
            .forUrl(Url.evaluation.rawValue)
            .withMethod(.Get)
            .withCallBack(_callback: callback)
            .create()
        )
    }
    
    func getBuyTogether(callback: @escaping callback<[ProductItemDTO]>) {
        ThreadRequest.execute(threadParams: RequestBuilder<[ProductItemDTO]>()
            .forUrl(Url.buyTogether.rawValue)
            .withMethod(.Get)
            .withCallBack(_callback: callback)
            .create()
        )
    }
    
    func getSawBought(callback: @escaping callback<[ProductItemDTO]>) {
        ThreadRequest.execute(threadParams: RequestBuilder<[ProductItemDTO]>()
            .forUrl(Url.sawBough.rawValue)
            .withMethod(.Get)
            .withCallBack(_callback: callback)
            .create()
        )
    }
    
    func getgetAlsoSaw(callback: @escaping callback<[ProductItemDTO]>) {
        ThreadRequest.execute(threadParams: RequestBuilder<[ProductItemDTO]>()
            .forUrl(Url.alsoSaw.rawValue)
            .withMethod(.Get)
            .withCallBack(_callback: callback)
            .create()
        )
    }
    
    func getProduct(callback:  @escaping callback<ProductDetailDTO>){
        ThreadRequest.execute(threadParams: RequestBuilder<ProductDetailDTO>()
            .forUrl(Url.productDetail.rawValue)
            .withMethod(.Get)
            .withCallBack(_callback: callback)
            .create()
        )
    }
    

}
