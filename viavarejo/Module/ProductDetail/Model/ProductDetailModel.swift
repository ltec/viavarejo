//
//  ProductDetailModel.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

struct ProductDetailDTO : Codable {
    
    var id: Int
    var name: String
    var model: ModelDTO
    var description: String
    var info: [InfoDTO]
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "nome"
        case model = "modelo"
        case description = "descricao"
        case info = "maisInformacoes"
    }
}

struct ProductDetailPriceDTO : Codable {
    var paymentPlain: String
    var price: Double
    var oldPrice: Double
    var discountPercentage: Int
    
    enum CodingKeys: String, CodingKey {
        case paymentPlain = "planoPagamento"
        case price = "precoAtual"
        case oldPrice = "precoAnterior"
        case discountPercentage = "porcentagemDesconto"
    }
}

struct ServiceModel : Codable {
    var name: String
    var price: String
    
    enum CodingKeys: String, CodingKey {
        case name = "nome"
        case price = "parcelamento"
    }
}

struct ModelDTO: Codable {
    var defaultt: DefaultDTO
    
    enum CodingKeys: String, CodingKey {
        case defaultt = "padrao"
    }
}

struct DefaultDTO: Codable {
    var images: [ImageDTO]
    var price: ProductDetailPriceDTO
    var services: [ServiceModel]
    var marketplace: MarketPlaceDTO
    
    enum CodingKeys: String, CodingKey {
        case images = "imagens"
        case price = "preco"
        case services = "servicos"
        case marketplace = "marketplace"
    }
}

struct MarketPlaceDTO: Codable {
    var logistDefault: LogistDTO
    
    enum CodingKeys: String, CodingKey {
        case logistDefault = "lojistaPadrao"
    }
}

struct LogistDTO: Codable {
    var name: String
    var price: Double
    
    enum CodingKeys: String, CodingKey {
        case name = "nome"
        case price = "preco"
    }
}

struct ImageDTO: Codable {
    var url: String
    
    enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}

struct InfoDTO: Codable {
    var description: String
    var values: [ValueDTO]
    
    enum CodingKeys: String, CodingKey {
        case description = "descricao"
        case values = "valores"
    }
}

struct ValueDTO: Codable {
    var name: String
    var value: String
    
    enum CodingKeys: String, CodingKey {
        case name = "nome"
        case value = "valor"
    }
}
