//
//  ProductItemDTO.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

struct ProductItemDTO : Codable {
    var name: String
    var imagemUrl: String
    var evaluation: Double
    var oldPrice: Double
    var price: Double
    var payment: String
    
    enum CodingKeys: String, CodingKey {
        case name = "nome"
        case evaluation = "classificacao"
        case imagemUrl = "imagemUrl"
        case price = "precoAtual"
        case oldPrice = "precoAnterior"
        case payment = "parcelamento"
    }
}
