//
//  EvaluationModel.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

struct EvaluationDTO: Codable {
    var evaluation: Double
    var qtd: Int
    var opinions: [OpinionDTO]
    
    enum CodingKeys: String, CodingKey {
        case evaluation = "classificacao"
        case qtd = "quantidade"
        case opinions = "opinioes"
    }
}

struct OpinionDTO: Codable {
    var client: String
    var date: String
    var comment: String
    var note: Int
    
    enum CodingKeys: String, CodingKey {
        case client = "cliente"
        case date = "data"
        case comment = "comentario"
        case note = "nota"
    }
}
