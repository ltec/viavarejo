//
//  ProductListFilterViewController.swift
//  viaverejo
//
//  Created by Ti Corporativo on 13/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol ProductListFilterViewControllerDelegate {
    func applyFilter(filter: ProductListFilterModel)
}

class ProductListFilterViewController: BaseViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var minPriceTextFieldView: TextFieldView!
    @IBOutlet weak var maxPriceTextFieldView: TextFieldView!
    @IBOutlet weak var applyButton: ButtonView!
    @IBOutlet weak var evaluationView: EvaluationView!
    @IBOutlet weak var evaluationLabel: UILabel!
    
    var flowDelegate: ProductListFilterViewControllerDelegate?
    var filterModel: ProductListFilterModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStyle()
        restoreStateFilter()
    }
    
    func restoreStateFilter(){
        if let filterModel = self.filterModel {
            evaluationView.evaluation = filterModel.evaluation
            if let minPrice = filterModel.minPrice {
                minPriceTextFieldView.textField.text = minPrice.parseCurrency()
            }
            
            if let maxPrice = filterModel.maxPrice {
                maxPriceTextFieldView.textField.text = maxPrice.parseCurrency()
            }
        }else {
            evaluationView.evaluation = 5
        }
    }
    
    func setupStyle(){
        self.view.backgroundColor = PalleteColor.greySecondary
        contentView.backgroundColor = PalleteColor.primary
        
        minPriceTextFieldView.type = .Decimal
        maxPriceTextFieldView.type = .Decimal
        
        evaluationView.type = .Action
        evaluationLabel.font = UIFont.bodyRegular()
    }
    
    
    @IBAction func applyFilter_didSelected(_ sender: Any) {
        guard let flowDelegate = self.flowDelegate else { return }
        
        let evaluation = evaluationView.evaluation
        
        guard let minPriceText = minPriceTextFieldView.textField.text else { return }
        guard let maxPriceText = maxPriceTextFieldView.textField.text else { return }
        
        let minPrice = minPriceText.isEmpty ? nil : minPriceText.toDoubleCurrencyValue()
        let maxPrice = maxPriceText.isEmpty ? nil : maxPriceText.toDoubleCurrencyValue()
        
        self.filterModel = ProductListFilterModel(evaluation: evaluation, minPrice: minPrice, maxPrice: maxPrice)
        flowDelegate.applyFilter(filter: filterModel!)
    }
    
}
