//
//  ProductListFilterCoordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 13/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol ProductListFilterCoordinatorDelegate {
    func applyFilter(filter: ProductListFilterModel)
}

class ProductListFilterCoordinator: Coordinator {
    weak var parentCoordinator: Coordinator?
    
    var flowDelegate: ProductListFilterCoordinatorDelegate?
    private let navigationController: UINavigationController
    private var productListFilterViewController: ProductListFilterViewController?
    var childrenCoordinator: [String : Coordinator]?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let productListFilterViewController = ProductListFilterViewController()
        productListFilterViewController.title = "Filtro"
        productListFilterViewController.flowDelegate = self
        productListFilterViewController.coordinator = self
        self.navigationController.pushViewController(productListFilterViewController, animated: true)
        self.productListFilterViewController = productListFilterViewController
    }
    
    func stateFilter(filterModel: ProductListFilterModel){
        self.productListFilterViewController?.filterModel = filterModel
    }
}

extension ProductListFilterCoordinator : ProductListFilterViewControllerDelegate {
    func applyFilter(filter: ProductListFilterModel) {
        guard let flowDelegate = self.flowDelegate else {return}
        flowDelegate.applyFilter(filter: filter)
        self.navigationController.popViewController(animated: true)
    }
}
