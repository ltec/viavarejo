//
//  ProductListFilterModel.swift
//  viaverejo
//
//  Created by Ti Corporativo on 13/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

struct ProductListFilterModel {
    var evaluation: Int
    var minPrice: Double?
    var maxPrice: Double?
}
