//
//  MainCoordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol MainCoordinatorDelegate {
    var viewController: UIViewController? { get }
}

class MainCoordinator: Coordinator {
    weak var parentCoordinator: Coordinator?
    
    var childrenCoordinator: [String : Coordinator]?
    
    private let navigationController: UINavigationController
    
    private var mainViewController: MainViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let mainViewController = MainViewController()
        mainViewController.flowDelegate = self
        
        getHome()
        getProduct()
        
        guard let homeCoordinator = self.getChildrenCoordinator(HomeCoordinator.self) as? HomeCoordinator else {return }
        
        guard let productListCoordinator = self.getChildrenCoordinator(ProductListCoordinator.self) as? ProductListCoordinator else {return }
        
        if let homeVC = homeCoordinator.viewController, let productListVC = productListCoordinator.viewController {
            mainViewController.controllers = [homeVC, productListVC]
        }
        
        navigationController.pushViewController(mainViewController, animated: true)
        self.mainViewController = mainViewController
    }
    
    func getHome() {
        let homeTabBarItem = UITabBarItem(title: MainEnum.home.getRawValue, image: UIImage(named: "home"), tag: 0)
        let homeCoordinator = HomeCoordinator(navigationController: self.navigationController, tabBarItem: homeTabBarItem)
        homeCoordinator.start()
        homeCoordinator.parentCoordinator = self
        setChildrenCoordinator(coordinator: homeCoordinator)
    }
    
    func getProduct() {
        let productTabBarItem = UITabBarItem(title: MainEnum.products.getRawValue, image: UIImage(named: "smart_tv"), tag: 1)
        let productListCoordinator = ProductListCoordinator(navigationController: self.navigationController, tabBarItem: productTabBarItem)
        productListCoordinator.start()
        productListCoordinator.parentCoordinator = self
        setChildrenCoordinator(coordinator: productListCoordinator)
    }
}

extension MainCoordinator : MainViewControllerDelegate {
    func getNavItem(navigation: UINavigationItem) {
        let navItem = self.navigationController.navigationBar.topItem
        navItem?.title = navigation.title
        navItem?.leftBarButtonItems = navigation.leftBarButtonItems
        navItem?.rightBarButtonItems = navigation.rightBarButtonItems
    }
}
