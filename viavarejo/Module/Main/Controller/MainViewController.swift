//
//  MainViewController.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol MainViewControllerDelegate {
    func getNavItem(navigation: UINavigationItem)
}

class MainViewController : UITabBarController {
    
    var controllers: [UIViewController] = []
    var flowDelegate: MainViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStyle()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.viewControllers == nil {
            self.setViewControllers(controllers, animated: false)
            guard let flowDelegate = self.flowDelegate else { return }
            let navItem = self.controllers[0].navigationItem
            flowDelegate.getNavItem(navigation: navItem)
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let flowDelegate = self.flowDelegate else { return }
        let navItem = self.controllers[item.tag].navigationItem
        flowDelegate.getNavItem(navigation: navItem)
    }
    
    func setupStyle(){
        self.tabBar.isTranslucent = false
        self.tabBar.tintColor = PalleteColor.secondary
    }
}
