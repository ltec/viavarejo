//
//  ProductlistCoordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductListCoordinator: Coordinator, MainCoordinatorDelegate {
    weak var parentCoordinator: Coordinator?
    
    private let navigationController: UINavigationController
    
    var childrenCoordinator: [String : Coordinator]?
    
    var viewController: UIViewController? {
        return self.productListViewController
    }
    
    private var productListViewController: ProductListViewController?
    private var tabBarItem: UITabBarItem
    
    init(navigationController: UINavigationController, tabBarItem: UITabBarItem) {
        self.navigationController = navigationController
        self.tabBarItem = tabBarItem
    }
    
    func start() {
        let productListViewController = ProductListViewController()
        productListViewController.flowDelegate = self
        productListViewController.tabBarItem = self.tabBarItem
        productListViewController.coordinator = self
        productListViewController.title = "Smart TV"
        
        let filterNavBarItem = UIBarButtonItem(image: UIImage(named: "filter"), style: .plain, target: self, action: #selector(getFilterViewController))
        filterNavBarItem.tintColor = PalleteColor.primary
        
        productListViewController.navigationItem.rightBarButtonItems = [filterNavBarItem]
        
        self.productListViewController = productListViewController
    }
    
    @objc func getFilterViewController(){
        let productListFilterCoordinator = ProductListFilterCoordinator(navigationController: self.navigationController)
        productListFilterCoordinator.start()
        productListFilterCoordinator.flowDelegate = self
        productListFilterCoordinator.parentCoordinator = self
        if let filterModel = productListViewController?.productCollectionView.filter {
            productListFilterCoordinator.stateFilter(filterModel: filterModel)
        }
        self.setChildrenCoordinator(coordinator: productListFilterCoordinator)
    }
}

extension ProductListCoordinator : ProductListViewControllerDelegate {
    func didSelectItemAt() {
        let productDetailCoordinator = ProductDetailCoordinator(navigationController: self.navigationController)
        productDetailCoordinator.start()
        productDetailCoordinator.parentCoordinator = self
        self.setChildrenCoordinator(coordinator: productDetailCoordinator)
    }
}

extension ProductListCoordinator : ProductListFilterCoordinatorDelegate {
    func applyFilter(filter: ProductListFilterModel) {
        self.productListViewController?.productCollectionView.filter = filter
    }
}
