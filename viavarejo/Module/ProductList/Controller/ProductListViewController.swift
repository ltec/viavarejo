//
//  ProductListViewController.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

enum SortProductEnum: Int {
    case MinPrice = 0
    case MaxPrice = 1
    case TopRated = 2
    
    var getRawValue: Int {
        return self.rawValue
    }
}

protocol ProductListViewControllerDelegate {
    func didSelectItemAt()
}

class ProductListViewController: BaseViewController {
    private var productService: ProductListServiceProtocol!
    private var alertService: AlertServiceProtocol!
    var flowDelegate: ProductListViewControllerDelegate?
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var productCollectionView: ProductCollectionView!
    
    var sortItems = ["Menor Preço", "Maior Preço", "Melhor Avaliado"]
    
    init(productService: ProductListServiceProtocol = ProductListService(), alertService: AlertServiceProtocol = AlertService()){
        
        super.init(nibName: nil, bundle: nil)
        self.productService = productService
        alertService.start(controller: self)
        self.alertService = alertService
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "productListView"
        self.getProducts()
        
        self.headerView.flowDelegate = self
        self.headerView.groupByButtonView.flowDelegate = self
        self.productCollectionView.flowDelegate = self
        self.alertService.showAlertLoading(completion: nil)
    }
    
    func getProducts(){
        self.productService.getProduct(callback: {(result, error) in
            if let items = result {
                self.productCollectionView.items = items.products
                self.sortItems(index: SortProductEnum.TopRated.getRawValue)
                self.headerView.qtdLabel.text = "\(items.qtd) ofertas"
            }
            self.alertService.hideAlertLoading()
        })
    }
    
    func sortItems(index: Int){
        let items = self.productCollectionView.items
        switch index {
        case SortProductEnum.MinPrice.getRawValue :
            self.productCollectionView.items = items.sorted{ $1.price.price > $0.price.price }
            break
        case SortProductEnum.MaxPrice.getRawValue :
            self.productCollectionView.items = items.sorted{ $1.price.price < $0.price.price }
            break
        default:
            self.productCollectionView.items = items.sorted{ $1.classification < $0.classification }
        }
        self.headerView.textFieldView.textField.text = self.sortItems[index]
    }
}

extension ProductListViewController : AlertViewDelegate {    
    
    func didSelected(index: Int) {
        self.alertService.showAlertLoading(completion: {
            self.sortItems(index: index)
            self.alertService.hideAlertLoading()
        })
    }
}

extension ProductListViewController : SortViewDelegate {
    func showAlertSort() {
        self.alertService.showAlertListView(title: "Ordenar por:", message: nil, items: self.sortItems)
    }
}

extension ProductListViewController : GroupByButtonViewDelegate {
    func groupBy(type: GroupByEnum) {
        self.productCollectionView.groupBy = type
    }
}

extension ProductListViewController : ProductCollectionViewDelegate {
    func didSelectItemAt() {
        guard let flowDelegate = self.flowDelegate else {return}
        flowDelegate.didSelectItemAt()
    }
}
