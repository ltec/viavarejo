//
//  ProductListService.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

protocol ProductListServiceProtocol {
    func getProduct(callback:  @escaping (ProductDTO?, Error?) -> Void)
}

class ProductListService : ProductListServiceProtocol {
    func getProduct(callback: @escaping (ProductDTO?, Error?) -> Void) {
        ThreadRequest.execute(threadParams: RequestBuilder<ProductDTO>()
            .forUrl(Url.productlist.rawValue)
            .withMethod(.Get)
            .withCallBack(_callback: {(result, error) in
                callback(result, error)
            })
            .create()
        )
    }
}
