//
//  ProductModel.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

struct ProductDTO : Codable {
    var products: [ProductModel]
    var qtd: Int
    
    enum CodingKeys: String, CodingKey {
        case products = "produtos"
        case qtd = "quantidade"
    }
}

struct ProductModel : Codable {
    var id: Int
    var sku: Int
    var name: String
    var isAvailable: Bool
    var description: String
    var imageUrl: String
    var classification: Int
    var price: PriceModel
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case sku = "sku"
        case name = "nome"
        case isAvailable = "disponivel"
        case description = "descricao"
        case imageUrl = "imagemUrl"
        case classification = "classificacao"
        case price = "preco"
    }
}

struct PriceModel : Codable {
    var paymentPlain: String
    var valueParcel: Double
    var qtdMaxParcel: Int
    var price: Double
    var oldPrice: Double
    var discountPercentage: Int
    var discountPaymentMethod: Double?
    
    enum CodingKeys: String, CodingKey {
        case paymentPlain = "planoPagamento"
        case valueParcel = "valorParcela"
        case qtdMaxParcel = "quantidadeMaximaParcelas"
        case price = "precoAtual"
        case oldPrice = "precoAnterior"
        case discountPercentage = "porcentagemDesconto"
        case discountPaymentMethod = "descontoFormaPagamento"
    }
}
