//
//  HomeCoordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class HomeCoordinator: Coordinator, MainCoordinatorDelegate {
    weak var parentCoordinator: Coordinator?
    
    private let navigationController: UINavigationController
    
    var childrenCoordinator: [String : Coordinator]?
    
    var viewController: UIViewController? {
        return self.homeViewController
    }
    
    private var homeViewController: HomeViewController?
    private var tabBarItem: UITabBarItem
    
    init(navigationController: UINavigationController, tabBarItem: UITabBarItem) {
        self.navigationController = navigationController
        self.tabBarItem = tabBarItem
    }
    
    func start() {
        let homeViewController = HomeViewController()
        homeViewController.tabBarItem = self.tabBarItem
        homeViewController.title = "Bem-Vindo"
        homeViewController.coordinator = self
        self.homeViewController = homeViewController
    }
}
