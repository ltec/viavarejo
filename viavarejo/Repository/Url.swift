//
//  Config.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 17/08/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import Foundation

enum Url : String {
    case productlist = "5d1b4f0f34000074000006dd"
    case productDetail = "5d1b4fd23400004c000006e1"
    case evaluation = "5d1b4f9134000078000006e0"
    case buyTogether = "5d1b50063400000f000006e7"
    case sawBough = "5d1b505134000074000006ec"
    case alsoSaw = "5d1b507634000054000006ed"
}

public enum StatusCodeEnum: Int {
    case Success = 200
    case BadRequest = 400
    case NoAuthorized = 401
    case Forbidden = 403
    case NotFound = 404
    case ServerError = 500
}
