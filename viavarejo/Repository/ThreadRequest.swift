//
//  ThreadRequest.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

struct ThreadParams<T: Codable> {
    var url: URL
    var body: String?
    var query: [String: String]?
    var method: MethodEnum
    var callBack: ((_ reponse: T?, _ error: Error?) -> Void?)
    var headers: [String: String]?
}

class ThreadRequest {
    static func execute <T: Codable> (threadParams: ThreadParams<T>){
        var request = URLRequest(url: threadParams.url)
        request.httpMethod = threadParams.method.getValue()
        
        if let data = threadParams.body, threadParams.method != .Get {
            request.httpBody = data.data(using: .utf8, allowLossyConversion: false)
        }
        
        request.allHTTPHeaderFields = threadParams.headers
        
        let dataTask = Config.session.dataTask(with: request) {
            (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    threadParams.callBack(nil, error)
                    return
                }
                guard let response = response else {
                    threadParams.callBack(nil, RequestServiceError.emptyResponse)
                    return
                }
                guard let data = data else {
                    threadParams.callBack(nil, RequestServiceError.emptyBody(response: response))
                    return
                }
                if data.isEmpty {
                    threadParams.callBack(nil, RequestServiceError.emptyBody(response: response))
                    return
                }
                
                print("REST RESPONSE: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
                do {
                    
                    guard let response = response as? HTTPURLResponse else {
                        threadParams.callBack(nil, RequestServiceError.emptyBody(response: nil))
                        return
                    }
                    
                    if response.statusCode == 200 {
                        let jsonDecoder = JSONDecoder()
                        let responseModel = try jsonDecoder.decode(T.self, from: data)
                        
                        threadParams.callBack(responseModel, nil)
                    } else if response.statusCode == 403 {
                        threadParams.callBack(nil, RequestServiceError.error(statusCode: StatusCodeEnum.Forbidden))
                    } else if response.statusCode == 401 {
                        threadParams.callBack(nil, RequestServiceError.error(statusCode: StatusCodeEnum.NoAuthorized))
                    } else if response.statusCode == 400 {
                        threadParams.callBack(nil, RequestServiceError.error(statusCode: StatusCodeEnum.BadRequest))
                    } else if response.statusCode == 500 {
                        threadParams.callBack(nil, RequestServiceError.error(statusCode: StatusCodeEnum.ServerError))
                    } else {
                        threadParams.callBack(nil, error)
                    }
                } catch let error as DecodingError {
                    threadParams.callBack(nil, RequestServiceError.decodingError(error: error))
                } catch let error {
                    threadParams.callBack(nil, error)
                }
            }
        }
        dataTask.resume()
    }
}
