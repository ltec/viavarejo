//
//  Config.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 17/08/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import Foundation

class Config {
    static let baseUrl = "https://www.mocky.io/v2/"
    
    private static let configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = false
        config.httpAdditionalHeaders = ["Content-Type":"application/json"]
        config.httpMaximumConnectionsPerHost = 5
        config.timeoutIntervalForRequest = 30.0
        return config
    }()
    
    public static let session = URLSession.shared //URLSession(configuration: configuration)
}

enum MethodEnum: String {
    case Post = "POST"
    case Get = "GET"
    
    func getValue() -> String {
        return self.rawValue
    }
}
