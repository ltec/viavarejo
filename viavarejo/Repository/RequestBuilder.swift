//
//  LoginRepository.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 17/08/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import Foundation

class RequestBuilder <T: Codable> {
    
    private var query: [String: String]?
    private var url: String?
    private var method: MethodEnum?
    private var callback: ((_ returnObject: T?, _ error: Error?) -> Void)?
    private var body: [String: String]?
    private var headers: [String: String]?
    
    func withQuery(_ query: [String : String]) -> RequestBuilder{
        self.query = query
        return self
    }
    
    func forUrl(_ route: String ) -> RequestBuilder {
        self.url = "\(Config.baseUrl)\(route)"
        return self
    }
    
    func withMethod(_ method: MethodEnum) -> RequestBuilder {
        self.method = method
        return self
    }
    
    func withCallBack(_callback :((_ returnObject: T?, _ error: Error?) -> Void)?) -> RequestBuilder{
        self.callback = _callback
        return self
    }
    
    func withBody(_ body: [String:String]?) -> RequestBuilder{
        self.body = body
        return self
    }
    
    public func withJsonHeader() -> RequestBuilder {
        self.headers = ["Accept":"application/json","Content-Type":"application/json"]
        return self
    }
    
    public func withHeader(_ headers: [String: String]?) -> RequestBuilder {
        self.headers = headers
        return self
    }
    
    func create() -> ThreadParams<T> {
        if let callback = self.callback {
            guard let path = self.url else {
                callback(nil, RequestServiceError.emptyUrl)
                fatalError("Parâmetro url inválido")
            }
            
            guard let url = URL(string: path) else {
                callback(nil, RequestServiceError.errorUrl)
                fatalError("Parâmetro url inválido")
            }
            
            guard let method = self.method else {
                callback(nil, RequestServiceError.emptyMethod)
                fatalError("Parâmetro method inválido")
            }
            
            if method != .Get {
                guard let body = self.body else {
                    callback(nil, RequestServiceError.emptyBodyParam)
                    fatalError("Parâmetro body inválido")
                }
     
                guard let json = try? JSONSerialization.data(withJSONObject: body, options: []) else {
                    callback(nil, RequestServiceError.errorEncodeBody)
                    fatalError("Parâmetro data inválido")
                }
                
                let data = String(data: json, encoding: String.Encoding.utf8)
                
                guard let headers = self.headers else {
                    callback(nil, RequestServiceError.errorEncodeBody)
                    fatalError("Parâmetro header inválido")
                }
                
                print("BODY: \(String(describing: String(data: json, encoding: String.Encoding.utf8)))")
                
                return ThreadParams(url: url, body: data, query: nil, method: method, callBack: callback, headers: headers)
            }else {
                return ThreadParams(url: url, body: nil, query: nil, method: method, callBack: callback, headers: nil)
            }
        }else {
            fatalError("Parâmetro callback inválido")
        }
    }
}
