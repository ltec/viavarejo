//
//  Coordinator.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var identifier: String { get }
    var parentCoordinator: Coordinator? { get set }
    var childrenCoordinator: [String: Coordinator]? { get set }
    func start()
    func childDidFinish(coordinator: Coordinator)
    func setChildrenCoordinator(coordinator: Coordinator)
    func getChildrenCoordinator(_ clas: AnyClass) -> Coordinator?
}

extension Coordinator {
    var identifier: String {
        return NSStringFromClass(Self.self)
    }
    
    func childDidFinish(coordinator: Coordinator) {
        if var childrenCoordinator = self.childrenCoordinator {
            childrenCoordinator[coordinator.identifier] = nil
        }
    }
    
    func getChildrenCoordinator(_ clas: AnyClass) -> Coordinator? {
        guard let childrenCoordinator = self.childrenCoordinator else {
            return nil
        }
        return childrenCoordinator[NSStringFromClass(clas)]
    }
    
    func setChildrenCoordinator(coordinator: Coordinator){
        if self.childrenCoordinator != nil {
            self.childrenCoordinator?[coordinator.identifier] = coordinator
        } else {
            self.childrenCoordinator = [coordinator.identifier : coordinator]
        }
    }
}
