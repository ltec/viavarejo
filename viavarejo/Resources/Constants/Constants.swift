//
//  Constants.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

enum MainEnum : String {
    case home = "Início"
    case products = "Produtos"
    
    var getRawValue: String {
        return self.rawValue
    }
}

enum ProductListEnum : String {
    case title = "Produtos"
    
    var getRawValue: String {
        return self.rawValue
    }
}

typealias callback<T> = (T?, Error?) -> Void
