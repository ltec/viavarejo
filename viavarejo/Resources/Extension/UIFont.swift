//
//  UIFont.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//
import UIKit
public struct FontNames {
    static let regular = "Roboto-Regular"
    static let bold = "Roboto-Bold"
}
public extension UIFont {
    class func headline() -> UIFont {
        guard let font = UIFont(name: FontNames.regular, size: 24) else {
            return UIFont.systemFont(ofSize: 24)
        }
        return font
    }
    
    class func headlineTwo() -> UIFont {
        guard let font = UIFont(name: FontNames.bold, size: 20) else {
            return UIFont.systemFont(ofSize: 20)
        }
        return font
    }
    
    class func headlineThree() -> UIFont {
        guard let font = UIFont(name: FontNames.regular, size: 20) else {
            return UIFont.systemFont(ofSize: 20)
        }
        return font
    }
    
    class func headlineFour() -> UIFont {
        guard let font = UIFont(name: FontNames.bold, size: 16) else {
            return UIFont.boldSystemFont(ofSize: 16)
        }
        return font
    }
    
    class func headlineFive() -> UIFont {
        guard let font = UIFont(name: FontNames.regular, size: 16) else {
            return UIFont.systemFont(ofSize: 16)
        }
        return font
    }
    
    class func bodyRegular() -> UIFont {
        guard let font = UIFont(name: FontNames.regular, size: 14) else {
            return UIFont.systemFont(ofSize: 14)
        }
        return font
    }
    
    class func customBody(size: CGFloat? = 16) -> UIFont {
        guard let size = size,
            let font = UIFont(name: FontNames.regular, size: size) else {
                return UIFont.systemFont(ofSize: 16)
        }
        return font
    }
    
    class func bodyBold() -> UIFont {
        guard let font = UIFont(name: FontNames.bold, size: 14) else {
            return UIFont.boldSystemFont(ofSize: 14)
        }
        return font
    }
    
    class func captionRegular() -> UIFont {
        guard let font = UIFont(name: FontNames.regular, size: 12) else {
            return UIFont.systemFont(ofSize: 12)
        }
        return font
    }
    
    class func captionBold() -> UIFont {
        guard let font = UIFont(name: FontNames.bold, size: 12) else {
            return UIFont.systemFont(ofSize: 12)
        }
        return font
    }
    
    class func overlineBold() -> UIFont { //uppercased
        guard let font = UIFont(name: FontNames.bold, size: 10) else {
            return UIFont.boldSystemFont(ofSize: 10)
        }
        return font
    }
    
    class func overlineRegular() -> UIFont { //uppercased
        guard let font = UIFont(name: FontNames.regular, size: 10) else {
            return UIFont.systemFont(ofSize: 10)
        }
        return font
    }
    
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
    func setBold() -> UIFont {
        if isBold {
            return self
        } else {
            var fontAtrAry = fontDescriptor.symbolicTraits
            fontAtrAry.insert([.traitBold])
            let fontAtrDetails = fontDescriptor.withSymbolicTraits(fontAtrAry)
            return UIFont(descriptor: fontAtrDetails!, size: pointSize)
        }
    }
    
    func setItalic()-> UIFont {
        if isItalic {
            return self
        } else {
            var fontAtrAry = fontDescriptor.symbolicTraits
            fontAtrAry.insert([.traitItalic])
            let fontAtrDetails = fontDescriptor.withSymbolicTraits(fontAtrAry)
            return UIFont(descriptor: fontAtrDetails!, size: pointSize)
        }
    }
    
    func desetBold() -> UIFont {
        if !isBold {
            return self
        }else{
            var fontAtrAry = fontDescriptor.symbolicTraits
            fontAtrAry.remove([.traitBold])
            let fontAtrDetails = fontDescriptor.withSymbolicTraits(fontAtrAry)
            return UIFont(descriptor: fontAtrDetails!, size: pointSize)
        }
    }
    
    func desetItalic()-> UIFont {
        if(!isItalic) {
            return self
        } else {
            var fontAtrAry = fontDescriptor.symbolicTraits
            fontAtrAry.remove([.traitItalic])
            let fontAtrDetails = fontDescriptor.withSymbolicTraits(fontAtrAry)
            return UIFont(descriptor: fontAtrDetails!, size: pointSize)
        }
    }
    
}
