//
//  Double.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

public extension Double {
    
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    func parseCurrency() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "R$ "
        numberFormatter.currencyDecimalSeparator = ","
        numberFormatter.currencyGroupingSeparator = "."
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.alwaysShowsDecimalSeparator = true
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = ","
        numberFormatter.groupingSeparator = "."
        var returnString = numberFormatter.string(from: NSNumber(value: self)) ?? ""
        
        if (self < 0) {
            returnString.insert("-", at: returnString.index(returnString.startIndex, offsetBy: 4))
            return String(returnString.dropFirst())
        }
        
        return returnString
    }
    
    func getStringWithDecimal(_ digits:Int = 2) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = ""
        numberFormatter.currencyDecimalSeparator = ","
        numberFormatter.currencyGroupingSeparator = "."
        numberFormatter.maximumFractionDigits = digits
        numberFormatter.minimumFractionDigits = digits
        numberFormatter.alwaysShowsDecimalSeparator = true
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = ","
        numberFormatter.groupingSeparator = "."
        let returnString = numberFormatter.string(from: NSNumber(value: self)) ?? ""
        return returnString
    }
    
    func getDoubleDigitString() -> String {
        return self.getStringWithDecimal()
    }
    
    func truncate(digits: Int = 2) -> Double {
        return Double(floor(pow(10.0, Double(digits)) * self)/pow(10.0, Double(digits)))
    }
    
    func roundForCurrency() -> Double {
        return (self * 100).rounded() / 100
    }
}
