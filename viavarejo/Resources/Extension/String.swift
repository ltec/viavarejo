//
//  String.swift
//  viaverejo
//
//  Created by Ti Corporativo on 13/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

extension String {
    func currencyFormating(_ allowEmptyValue:Bool? = nil) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "R$ "
        numberFormatter.currencyDecimalSeparator = ","
        numberFormatter.currencyGroupingSeparator = "."
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.alwaysShowsDecimalSeparator = true
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = ","
        numberFormatter.groupingSeparator = "."
        
        var amountWithPrefix = self
        var number: NSNumber!
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            if let allowEmptyValue = allowEmptyValue {
                return allowEmptyValue ? "R$ 0,00" : ""
            }
            return ""
        }
        
        return numberFormatter.string(from: number)!
    }
    
    func valueWithDecimalFormatting() -> String {
        let numbersOnly = self.getOnlyNumbers()
        let decimalDigits = 2
        if (numbersOnly.count >= 2) {
            return "\(numbersOnly.prefix(numbersOnly.count - decimalDigits)).\(numbersOnly.suffix(decimalDigits))"
        } else {
            return "0.00"
        }
    }
    
    func getOnlyNumbers() -> String {
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        return regex.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
    }
    
    func toDoubleCurrencyValue() -> Double? {
        let onlyNumberAndComma = self.filter { (char) -> Bool in
            return "0123456789,".contains(char)
        }
        let dotDecimal = onlyNumberAndComma.replacingOccurrences(of: ",", with: ".")
        let doubleValue = Double(dotDecimal)
        return doubleValue
    }
}
