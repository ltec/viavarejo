//
//  UIButton.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

extension UIButton {
    
    func configImage(image: UIImage?, tintColor: UIColor){
        _ = image?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: -4, left: -4, bottom: -4, right: -4)
        self.tintColor = tintColor
        self.titleLabel?.text = ""
    }
    
}
