//
//  UILabel.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

extension UILabel {
    func setup(font: UIFont, color: UIColor){
        self.font = font
        self.textColor = color
    }
    
    func setupGroupTitle(){
        self.font = UIFont.headlineThree()
        self.textColor = PalleteColor.bluePrimary
    }
}
