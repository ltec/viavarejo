//
//  Date.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation


public extension Date {
    static let ddMMyyyBRFormat: String = "dd/MM/yyyy"
    static let yyyyMMddFormat: String = "yyyy-MM-dd"
    static let yyyyMMddHHmmssFormat: String = "yyyy-MM-dd'T'HH:mm:ss"
    static let isoString: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    static let isoString8601: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let ddMMyyyFormat: String = "dd-MM-yyyy"
    
    static let meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    static var tomorrow:  Date { return Date().dayAfter }
    
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    func startOfMonth(format: String? = isoString) -> String {
        guard let returnDate = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self))),
            let format = format else { return "" }
        
        return returnDate.asString(withFormat: format)
    }
    
    func endOfMonth(format: String? = isoString) -> String {
        guard let returnDate = Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.dateFrom(string: startOfMonth(format: format), withFormat: format) ?? Date()),
            let format = format else { return "" }
        return returnDate.asString(withFormat: format)
    }
    
    func dateFrom(string: String, withFormat format: String? = isoString) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: string)
        
        return date
    }
    
    func stringFromDate(date: Date? = Date(), withFormat format: String? = isoString) -> String {
        guard let format = format else {
            return ""
        }
        return date?.asString(withFormat: format) ?? ""
    }
    
    func asString(withFormat format: String? = "dd/MM/yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //forçando o pt_BR pq Locale.current não retorna a string correta
        //        dateFormatter.locale = Locale(identifier: Locale.current.languageCode ?? "pt_BR")
        dateFormatter.locale = Locale(identifier: "pt_BR")
        return dateFormatter.string(from: self)
    }
    
    func getFormattedDateToSections() -> String {
        var formattedDate = ""
        
        let day = Calendar.current.component(.day, from: self)
        let month = Calendar.current.component(.month, from: self)
        let year = Calendar.current.component(.year, from: self)
        
        let todayDay = Calendar.current.component(.day, from: Date())
        let todayMonth = Calendar.current.component(.month, from: Date())
        let todayYear = Calendar.current.component(.year, from: Date())
        
        if day == todayDay, month == todayMonth, year == todayYear {
            formattedDate = "Hoje"
        }else {
            formattedDate = "\(String(format: "%02d", day)) de \(Date.meses[month-1])"
        }
        
        return formattedDate
    }
    
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }
    
    func getFormattedDateToMenuBar(fullMonth: Bool = false) -> String {
        let today = Date()
        
        if fullMonth {
            return asString(withFormat: "MMMM").uppercased()
        }
        
        if isInSameYear(date: today) {
            return asString(withFormat: "MMM").uppercased()
        }
        return asString(withFormat: "MMM/yy").uppercased()
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self))) ?? Date()
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth()) ?? Date()
    }
}
