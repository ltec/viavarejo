//
//  Pallete.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

open class PalleteColor : NSObject {
    
    public static var primary = UIColor.fromHex(0xffffff)
    public static var secondary = UIColor.fromHex(0x003967)
    public static var bluePrimary = UIColor.fromHex(0x2f7dc0)
    public static var greyPrimary = UIColor.fromHex(0xb3b3b3)
    public static var greySecondary = UIColor.fromHex(0xf9f9f9)
    public static var greenPrimary = UIColor.fromHex(0xadd482)
    public static var greenSecondary = UIColor.fromHex(0xf1ffe3)
    public static var yellowPrimary = UIColor.fromHex(0xffbc00)
    public static var yellowSecondary = UIColor.fromHex(0xffb417)
    public static var greenLima = UIColor.fromHex(0x73B628)
    public static var blueAlice = UIColor.fromHex(0xe6f1fb)
}
