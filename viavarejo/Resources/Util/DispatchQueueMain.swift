//
//  DispatchQueueApplication.swift
//  viaverejo
//
//  Created by Ti Corporativo on 22/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation

protocol DispatchQueueMainProtocol {
    func async(callback: @escaping (() -> Void))
}

class DispatchQueueMain: DispatchQueueMainProtocol {
    
    static var main: DispatchQueueMainProtocol = DispatchQueueMain()
    
    func async(callback: @escaping (() -> Void)) {
        DispatchQueue.main.async {
            callback()
        }
    }
}
