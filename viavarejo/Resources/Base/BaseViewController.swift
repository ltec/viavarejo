//
//  BaseViewController.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    weak var coordinator: Coordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let coordinator = self.coordinator, let parentCoordinator = coordinator.parentCoordinator {
            parentCoordinator.childDidFinish(coordinator: coordinator)
        }
    }

}
