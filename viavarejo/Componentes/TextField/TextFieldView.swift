//
//  SelectView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

enum TextFieldTypeEnum {
    case Select
    case Text
    case Decimal
    case Cep
}

@IBDesignable class TextFieldView : UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    private var isDeleting = false
    private var oldText = ""
    public var allowEmptyValue: Bool?
    var placeholder: String? {
        didSet {
            setupPlaceholder()
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBInspectable var title: String = "" {
        didSet {
            titleLabel.text = self.title
        }
    }
    var type: TextFieldTypeEnum = .Select {
        didSet {
            loadType()
        }
    }
    var isRequired: Bool = false
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupStyle()
        loadType()
        setupTextField()
    }
    
    func setupStyle(){
        contentView.layer.borderColor = PalleteColor.greyPrimary.cgColor
        contentView.layer.borderWidth = 1
        contentView.layer.cornerRadius = CGFloat(4)
        contentView.clipsToBounds = true
        
        let image = UIImage(named: "down_arrow")?
            .withAlignmentRectInsets(UIEdgeInsets(top: -4, left: -4, bottom: -4, right: -4))
            .withRenderingMode(.alwaysTemplate)
        iconImageView.image = image
        iconImageView.tintColor = PalleteColor.bluePrimary
        
        titleLabel.textColor = PalleteColor.secondary
        titleLabel.font = UIFont.bodyRegular()
    }
    
    func loadType(){
        switch self.type {
        case .Select:
            self.textField.isUserInteractionEnabled = false
            titleLabel.isHidden = true
            textField.delegate = nil
            break
        case .Decimal :
            configNumPad()
            self.textField.isUserInteractionEnabled = true
            titleLabel.isHidden = false
            self.iconImageView.isHidden = true
            self.textField.delegate = self
        default:
            self.textField.isUserInteractionEnabled = true
            titleLabel.isHidden = false
            self.iconImageView.isHidden = true
            self.textField.delegate = self
        }
    }
    
    private func configNumPad() {
        textField.keyboardType = .numberPad
    }
    
    private func setupTextField(){
        textField.addTarget(self, action: #selector(didEditTextfield(textField:)), for: .editingChanged)
        textField.autocorrectionType = UITextAutocorrectionType.no
        
        textField.textColor = PalleteColor.greyPrimary
        textField.font = UIFont.headlineFour()
    }
    
    private func setupToolBar() {
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(dismissKeyBoard))]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
    }
    
    @objc func dismissKeyBoard() {
        textField.resignFirstResponder()
    }
    
    @objc func didEditTextfield(textField: UITextField) {
        if let text = textField.text {
            var cursorOffset: Int = 0
            let selectRange = textField.selectedTextRange
            if let range = selectRange {
                cursorOffset = textField.offset(from: textField.beginningOfDocument, to: range.start)
            }
            var cursorPosition = textField.position(from: textField.beginningOfDocument, offset: cursorOffset)
            switch self.type {
            case .Decimal:
                if isDeleting {
                    cursorOffset += 1
                } else {
                    cursorOffset -= 1
                }
                textField.text = text.currencyFormating(allowEmptyValue)
                
                if (text.getOnlyNumbers().count > 11) {
                    textField.text = String(text.dropLast()).currencyFormating(allowEmptyValue)
                }
                
                let newDigits = (textField.text?.count ?? 0) - oldText.count
                var newPosition = cursorOffset + newDigits
                if newPosition < 0 {
                    newPosition = 0
                } else if newPosition < 3 && (textField.text?.count ?? 0) > 3 {
                    newPosition = 3
                } else if newPosition >= (textField.text?.count ?? 0) {
                    newPosition = textField.text?.count ?? 0
                } else if text.getOnlyNumbers().count == 11 && newPosition < 17 {
                    newPosition += 1
                } else if Int(text.getOnlyNumbers()) != 0 && oldText == textField.text {
                    newPosition -= 1
                }
                cursorPosition = textField.position(from: textField.beginningOfDocument, offset: newPosition)
                break
            default :
                break
            }
            if let cursor = cursorPosition {
                textField.selectedTextRange = textField.textRange(from: cursor, to: cursor)
            }
        }
    }
    
    func setupPlaceholder(){
        self.textField.placeholder = self.placeholder ?? ""
    }
}

extension TextFieldView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.contentView.layer.borderColor = PalleteColor.bluePrimary.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.contentView.layer.borderColor = PalleteColor.greyPrimary.cgColor
    }
}
