//
//  InfoActionTextFieldView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

@IBDesignable class InfoActionTextFieldView : UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textFieldView: TextFieldView!
    @IBOutlet weak var iconActionButton: UIButton!
    @IBOutlet weak var actionButton: ButtonView!
    @IBOutlet weak var iconTitleButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupTextField()
    }
    
    func setupTitleLabel(text: String, font: UIFont, color: UIColor){
        titleLabel.text = text
        titleLabel.setup(font: font, color: color)
    }
    
    func setupIconTitleButton(image: UIImage?, tintColor: UIColor, isHidden: Bool){
        iconTitleButton.configImage(image: image, tintColor: tintColor)
        iconTitleButton.isHidden = isHidden
        iconTitleButton.borderRadius(radius: 8, borderWidth: 1, borderColor: tintColor)
    }
    
    func setupTextField(){
        textFieldView.type = .Text
        textFieldView.textField.isUserInteractionEnabled = false
        textFieldView.titleLabel.isHidden = true
    }
    
    func setupIconActionButton(image: UIImage?, tintColor: UIColor, backgroundColor: UIColor){
        self.iconActionButton.configImage(image: image, tintColor: tintColor)
        self.iconActionButton.backgroundColor = backgroundColor
        self.iconActionButton.borderRadius(radius: 3, borderWidth: 0, borderColor: UIColor.clear)
    }
    
    func setupActionButton(text: String, color: UIColor, isHidden: Bool){
        self.actionButton.type = .Clear
        self.actionButton.size = .Small
        self.actionButton.referenceColor = color
        self.actionButton.isHidden = isHidden
        self.actionButton.setButtonTitle(text)
    }
    
    func setPlaceholder(placeholder: String){
        textFieldView.placeholder = placeholder
    }
}
