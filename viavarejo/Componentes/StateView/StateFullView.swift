//
//  StateFullView.swift
//  CaixaUI
//
//  Created by Wilson Kim on 09/05/19.
//

import UIKit

public enum ViewStateEnum {
    case loading(_ loadingMessage:String?)
    case error(_ errorMessage:String?)
    case empty(_ emptyMessage:String?)
    case defaultLayout
}

public class StateFullView: UIView {
    
    var errorView = ErrorView()
    var loadingView = LoadingView()
    var emptyView = EmptyView()
    
    public var state:ViewStateEnum = .defaultLayout {
        didSet {
            updateViewState()
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        setup()
    }
    
    private func setup() {
        setupErrorView()
        setupLoadingView()
        setupEmptyView()
    }
    
    private func setupErrorView() {
        setupView(errorView)
    }
    
    private func setupLoadingView() {
        setupView(loadingView)
    }
    
    private func setupEmptyView() {
        setupView(emptyView)
    }
    
    func setupView(_ view: UIView){
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        view.isHidden = true
    }
    
    private func updateViewState() {
        switch state {
        case let .error(message):
            errorView.message.text = message
            loadingView.stopLoading()
            errorView.isHidden = false
            loadingView.isHidden = true
            emptyView.isHidden = true
            break
        case let .loading(message):
            loadingView.setLoadingMessage(message)
            loadingView.startLoading()
            loadingView.isHidden = false
            errorView.isHidden = true
            emptyView.isHidden = true
            break
        case let .empty(message) :
            emptyView.setEmptyMessage(message)
            loadingView.stopLoading()
            loadingView.isHidden = true
            errorView.isHidden = true
            emptyView.isHidden = false
            break
        case .defaultLayout:
            loadingView.stopLoading()
            errorView.isHidden = true
            loadingView.isHidden = true
            emptyView.isHidden = true
            break
        }
    }
    
    public func setErrorCompletion(_ errorBlock:@escaping () -> Void) {
        errorView.setCompletion {
            errorBlock()
        }
    }
}
