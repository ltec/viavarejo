//
//  ActivityIndicatorView.swift
//  ECobranca
//
//  Created by Euler Carvalho on 08/01/19.
//  Copyright © 2019 Euler Carvalho. All rights reserved.
//

import UIKit

open class ActivityIndicatorView: NSObject {
    private static var containerView = UIView()
    private static var progressView = UIView()
    private static var activityIndicator = UIActivityIndicatorView()
    private static var addedLoadViewFrame: UIView?
    
    public static func show() {
        DispatchQueue.main.async {
            let window = UIWindow(frame: UIScreen.main.bounds)
            containerView.frame = window.frame
            containerView.center = window.center
            containerView.backgroundColor = UIColor.fromHex(0x444444, alpha: 0.3)
            
            progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            progressView.center = window.center
            progressView.clipsToBounds = true
            progressView.layer.cornerRadius = 10
            
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator.style = .whiteLarge
            activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
            
            progressView.addSubview(activityIndicator)
            containerView.addSubview(progressView)
            UIApplication.shared.keyWindow?.addSubview(containerView)
            
            activityIndicator.startAnimating()
        }
    }
    
    public static func showOnView(view: UIView, onViewFrame: UIView? = nil) {
        DispatchQueue.main.async {
            if let viewFrame = onViewFrame {
                addedLoadViewFrame = viewFrame
                containerView.frame = viewFrame.frame
            } else {
                containerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            }
            containerView.backgroundColor = UIColor.fromHex(0x444444, alpha: 0.3)
            
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator.style = .whiteLarge
            activityIndicator.center = CGPoint(x: containerView.bounds.width / 2, y: containerView.bounds.height / 2.3)
            
            containerView.addSubview(activityIndicator)
            view.addSubview(containerView)
            
            activityIndicator.startAnimating()
        }
    }
    
    public static func updateFrame() {
        if let addedLoadViewFrame = addedLoadViewFrame {
            containerView.frame = addedLoadViewFrame.frame
            activityIndicator.center = CGPoint(x: containerView.bounds.width / 2, y: containerView.bounds.height / 2.3)
        }
    }
    
    public static func hide() {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            containerView.removeFromSuperview()
            addedLoadViewFrame = nil
        }
    }
}
