//
//  LoadingView.swift
//  CaixaUI
//
//  Created by Wilson Kim on 09/05/19.
//

import UIKit

class LoadingView:UIView {
    
    
    @IBOutlet weak var colorBackgroundView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        loadViewFromNib()
        setup()
    }
    
    private func setup() {
        setupContainerView()
        setupLabel()
    }
    
    private func setupContainerView() {
        containerView.layer.cornerRadius = 16
        colorBackgroundView.backgroundColor = PalleteColor.greySecondary
    }
    
    private func setupLabel() {
        messageLabel.textColor = UIColor.black
        messageLabel.font = UIFont.headlineFive()
    }
    
    public func setLoadingMessage(_ message:String?) {
        messageLabel.text = message
    }
    
    public func startLoading() {
        activityIndicator.startAnimating()
    }
    
    public func stopLoading() {
        activityIndicator.stopAnimating()
    }
}

