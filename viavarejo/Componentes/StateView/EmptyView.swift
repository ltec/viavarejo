//
//  EmptyView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        loadViewFromNib()
        
        messageLabel.font = UIFont.headlineFive()
        messageLabel.textColor = PalleteColor.bluePrimary
    }
    
    public func setEmptyMessage(_ message:String?) {
        messageLabel.text = message
    }
}
