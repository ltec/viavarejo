//
//  StateViewController.swift
//  viaverejo
//
//  Created by Ti Corporativo on 18/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

public enum ViewControllerStateEnum {
    case loading
    case defaultLayout
}

public class StateViewController: UIView {
    
    var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = PalleteColor.greySecondary
        return view
    }()
    
    public var state:ViewControllerStateEnum = .loading {
        didSet {
            updateViewState()
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        setup()
    }
    
    private func setup() {
        self.addSubViewWithConstraints(subView: self.contentView)
    }
    
    
    private func updateViewState() {
        if state == .defaultLayout {
            self.contentView.isHidden = true
        } else {
            self.contentView.isHidden = false
        }
    }
}
