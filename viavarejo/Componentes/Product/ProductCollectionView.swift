//
//  ProductCollectionView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol ProductCollectionViewDelegate {
    func didSelectItemAt()
}

class ProductCollectionView : UICollectionView {
    lazy var imageService: ImageServiceProtocol = {
        return ImageService()
    }()
    var flowDelegate: ProductCollectionViewDelegate?
    var filter: ProductListFilterModel? {
        didSet {
            applyFilter()
            self.backgroundView = self.filteredItems.count == 0 ? self.emptyView : nil
            self.reloadData()
        }
    }
    var filteredItems:[ProductModel] = []
    var items:[ProductModel] = [] {
        didSet {
            self.filteredItems = self.items
            self.reloadData()
        }
    }
    var groupBy: GroupByEnum = .Column {
        didSet {
            self.reloadData()
        }
    }
    var cellIdenfier: String = "ProductCollectionViewCell"
    var emptyView = EmptyView()
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.accessibilityIdentifier = "productCollection"
        emptyView.setEmptyMessage("Nenhum oferta para o filtro aplicado.")
        
        register(UINib(nibName: cellIdenfier, bundle: nil), forCellWithReuseIdentifier: cellIdenfier)
        self.dataSource = self
        self.delegate = self
        self.backgroundColor = PalleteColor.greySecondary
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 1
            layout.minimumInteritemSpacing = 1
        }
        
        self.layer.borderWidth = 1
        self.layer.borderColor = PalleteColor.greySecondary.cgColor
        self.clipsToBounds = true
    }
    
    func applyFilter(){
        guard let filter = self.filter else {return}
        let items = self.items
        self.filteredItems = items.filter{ $0.classification <= filter.evaluation && $0.price.price >= filter.minPrice ?? $0.price.price && $0.price.price <= filter.maxPrice ?? $0.price.price }
    }
}

extension ProductCollectionView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: cellIdenfier, for: indexPath) as! ProductCollectionViewCell
        let item = self.filteredItems[indexPath.row]
        cell.imageService = self.imageService
        cell.bind(productModel: item)
        cell.groupBy = self.groupBy
        return cell
    }
}

extension ProductCollectionView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let flowDelegate = self.flowDelegate else {return}
        flowDelegate.didSelectItemAt()
    }
}

extension ProductCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  1
        let collectionViewSize = collectionView.frame.size.width - padding
        let width = self.groupBy == .Column ? collectionViewSize/2 : collectionViewSize
        let height: CGFloat = self.groupBy == .Column ? 320 : 320 - 128
        return CGSize(width: width, height: height)
    }
}
