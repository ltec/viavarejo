//
//  ProductHorizontalColllectionView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//
import UIKit

class ProductHorizontalCollectionView : UICollectionView {
    
    lazy var imageService: ImageServiceProtocol = {
        return ImageService()
    }()
    
    var items:[ProductItemDTO] = [] {
        didSet {
            self.reloadData()
        }
    }
    
    var cellIdenfier: String = "ProductCollectionViewCell"
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        register(UINib(nibName: cellIdenfier, bundle: nil), forCellWithReuseIdentifier: cellIdenfier)
        self.dataSource = self
        self.delegate = self
        self.backgroundColor = PalleteColor.primary
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 8
            layout.minimumInteritemSpacing = 8
        }
    }
}

extension ProductHorizontalCollectionView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: cellIdenfier, for: indexPath) as! ProductCollectionViewCell
        let item = self.items[indexPath.row]
        cell.imageService = self.imageService
        cell.bind(productItem: item)
        return cell
    }
}

extension ProductHorizontalCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame.size.width
        let width = collectionViewSize * 0.4
        let height: CGFloat = 320
        return CGSize(width: width, height: height)
    }
}
