//
//  ProductCollectionContentView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductCollectionContentView : UIView {
    
    @IBInspectable var title: String = "" {
        didSet {
            self.titleLabel.text = self.title
        }
    }
    
    @IBOutlet weak var stateView: StateFullView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: ProductHorizontalCollectionView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        stateView.state = .loading("Carregando...")
        
        stylelabel()
    }
    
    func bind(items: [ProductItemDTO]){
        collectionView.items = items
        stateView.state = .defaultLayout
    }
    
    func stateError(){
        stateView.state = .error("Não foi possível listar os produtos.")
    }
    
    func stylelabel(){
        titleLabel.setup(font: UIFont.headlineFour(), color: PalleteColor.secondary)
    }
}
