//
//  PriceView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

@IBDesignable class PriceView : UIView {
    
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var parcelLabel: UILabel!
    @IBOutlet weak var discountLabel: DiscountLabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupFont()
        setupColor()
    }
    
    func setupFont(){
        oldPriceLabel.font = UIFont.bodyRegular()
        priceLabel.font = UIFont.headlineTwo()
        parcelLabel.font = UIFont.captionRegular()
    }
    
    func setupColor(){
        oldPriceLabel.textColor = PalleteColor.greyPrimary
        priceLabel.textColor = PalleteColor.bluePrimary
        parcelLabel.textColor = PalleteColor.greenPrimary
    }
    
    func setupAccessibility(){
        if let oldPrice = self.oldPriceLabel.text, let price = self.priceLabel.text, let payment = self.parcelLabel.text {
            
            self.oldPriceLabel.accessibilityLabel = "De \(oldPrice.replacingOccurrences(of: "R$", with: "")) reais"
            self.priceLabel.accessibilityLabel = "Por \(price.replacingOccurrences(of: "R$", with: "")) reais"
            self.parcelLabel.accessibilityLabel = "Parcelado em \(payment.replacingOccurrences(of: "R$", with: "")) reais"
        }
    }
}
