//
//  ProductCollectionViewCell.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    var imageService: ImageServiceProtocol?
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var nameLabel: ProductNameLabel!
    @IBOutlet weak var evaluationView: EvaluationView!
    @IBOutlet weak var priceView: PriceView!
    var imageViewWidthConstraint: NSLayoutConstraint?
    var groupBy: GroupByEnum = .Column {
        didSet {
            updateLayoutCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup(){
        self.accessibilityIdentifier = "productCell"
        self.imageViewWidthConstraint = self.imageView.widthAnchor.constraint(equalToConstant: self.contentView.frame.width * 0.4)
    }
    
    func bind(productModel: ProductModel) {

        self.imageService?.downloadImage(from: productModel.imageUrl.replacingOccurrences(of: "http", with: "https"), callback: {(image) in
            DispatchQueueMain.main.async {
                self.imageView.image = image
            }
        })
        
        nameLabel.text = productModel.name
        priceView.oldPriceLabel.text = productModel.price.oldPrice.parseCurrency()
        priceView.priceLabel.text = productModel.price.price.parseCurrency()
        priceView.parcelLabel.text = productModel.price.paymentPlain
        
        evaluationView.evaluation = productModel.classification
        self.priceView.setupAccessibility()
        setupAccessibility()
    }
    
    func bind(productItem: ProductItemDTO){
        self.imageService?.downloadImage(from: productItem.imagemUrl.replacingOccurrences(of: "http", with: "https"), callback: {(image) in
            DispatchQueueMain.main.async {
                self.imageView.image = image
            }
        })
        
        nameLabel.text = productItem.name
        priceView.oldPriceLabel.text = productItem.oldPrice.parseCurrency()
        priceView.priceLabel.text = productItem.price.parseCurrency()
        priceView.parcelLabel.text = productItem.payment
        evaluationView.evaluation = Int(productItem.evaluation)
        self.priceView.setupAccessibility()
        setupAccessibility()
    }
    
    func updateLayoutCell(){
        if self.groupBy == .Column {
            contentStackView.axis = .vertical
            self.imageViewWidthConstraint?.isActive = false
        }else {
            contentStackView.axis = .horizontal
            self.imageViewWidthConstraint?.isActive = true
        }
    }
    
    func setupAccessibility(){
        self.isAccessibilityElement = true
        self.accessibilityTraits = UIAccessibilityTraits.selected
        self.accessibilityLabel = self.nameLabel.text
        self.accessibilityHint = "\(evaluationView.accessibilityLabel!) \(priceView.oldPriceLabel.accessibilityLabel!) \(priceView.priceLabel.accessibilityLabel!) \(priceView.parcelLabel.accessibilityLabel!)"
    }

}
