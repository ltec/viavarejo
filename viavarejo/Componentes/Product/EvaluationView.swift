//
//  EvaluationView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 11/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

enum EvaluationTypeEnum {
    case View
    case Action
}

@IBDesignable class EvaluationView: UIView {
    
    @IBOutlet var contentView: UIView!
    var evaluation: Int = 0 {
        didSet {
            loadEvaluation()
        }
    }

    var type: EvaluationTypeEnum = .View {
        didSet {
            loadType()
        }
    }
    private let numberStars = 4
    @IBOutlet weak var oneStarButton: UIButton!
    @IBOutlet weak var twoStarButton: UIButton!
    @IBOutlet weak var threeStarButton: UIButton!
    @IBOutlet weak var fourStarButton: UIButton!
    @IBOutlet weak var fiveStarButton: UIButton!
    var stars: [UIButton] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        self.stars = [oneStarButton, twoStarButton, threeStarButton, fourStarButton, fiveStarButton]
        setupImages()
        loadType()
        setupAccessibility()
    }
    
    func setupImages(){
        let image = UIImage(named: "star")?.withRenderingMode(.alwaysTemplate)
        for star in self.stars {
            star.setImage(image, for: .normal)
            star.imageView?.contentMode = .scaleAspectFit
        }
    }
    
    func setupEvents(){
        for index in 0...self.numberStars {
            let star = self.stars[index]
            star.tag = index + 1
            star.addTarget(self, action: #selector(starDidSelected), for: .touchUpInside)
            star.tintColor = PalleteColor.greyPrimary
        }
    }
    
    @objc func starDidSelected(sender:UIButton){
        self.evaluation = sender.tag
    }
    
    func loadEvaluation(){
        for index in 0...self.numberStars {
            self.stars[index].tintColor =  index < self.evaluation ? PalleteColor.yellowPrimary : PalleteColor.greyPrimary
        }
        self.accessibilityLabel = "Avaliação \(String(self.evaluation)) estrelas"
    }
    
    func loadType(){
        for index in 0...self.numberStars {
            self.stars[index].isUserInteractionEnabled = self.type == .View ? false : true
        }
        if self.type == .Action {
            setupEvents()
        }
    }
    
    func setupAccessibility(){
        for index in 0...self.numberStars {
            self.stars[index].isAccessibilityElement = false
        }
        self.isAccessibilityElement = true
    }
}
