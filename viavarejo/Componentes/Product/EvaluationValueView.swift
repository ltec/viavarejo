//
//  EvaluationValueView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class EvaluationValueView : UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var subTitleLabel: UILabel!
    
    @IBOutlet weak var lineView: LineView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        titleLabel.setup(font: UIFont.headline(), color: UIColor.black)
        subTitleLabel.setup(font: UIFont.headlineThree(), color: PalleteColor.greyPrimary)
        lineView.backgroundColor = PalleteColor.greyPrimary
    }
}
