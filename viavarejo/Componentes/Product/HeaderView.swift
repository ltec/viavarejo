//
//  SortView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol SortViewDelegate {
    func showAlertSort()
}

@IBDesignable class HeaderView : UIView {
    
    @IBOutlet weak var qtdLabel: UILabel!
    @IBOutlet weak var groupByButtonView: GroupByButtonView!
    @IBOutlet weak var textFieldView: TextFieldView!
    var flowDelegate: SortViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupStyle()
        setupEvent()
    }
    
    func setupEvent(){
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showAlertSort))
        self.addGestureRecognizer(gesture)
    }
    
    @objc func showAlertSort(){
        guard let flowDelegate = self.flowDelegate else {return}
        flowDelegate.showAlertSort()
    }
    
    func setupStyle(){
        qtdLabel.font = UIFont.bodyRegular()
        qtdLabel.textColor = PalleteColor.greyPrimary
        
        self.dropShadow(color: UIColor.black, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 4, scale: true)
    }
}
