//
//  UITitleSection.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 09/07/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import UIKit

@IBDesignable  class TitleSectionView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var label: UILabel!
    
    @IBInspectable var title: String = "" {
        didSet {
            label.text = self.title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.heightAnchor.constraint(equalToConstant: 49).isActive = true
        loadViewFromNib()
        setupStyle()
    }
    
    func setupStyle(){
        label.font = UIFont.headlineThree()
        label.textColor = PalleteColor.bluePrimary
        self.backgroundColor = UIColor.clear
    }
    
    public func setTitle(text: String){
        label.text = text
    }
}
