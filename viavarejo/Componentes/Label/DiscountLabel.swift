//
//  discountLabel.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class DiscountLabel : UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.font = UIFont.overlineBold()
        self.layer.cornerRadius = 10
        self.backgroundColor = PalleteColor.yellowSecondary
        self.clipsToBounds = true
        self.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
}
