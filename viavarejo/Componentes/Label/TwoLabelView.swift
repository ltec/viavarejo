//
//  TwoLabelView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class TwoLabelView : UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func setupTitle(text: String?, font: UIFont, color: UIColor){
        self.titleLabel.text = text
        self.titleLabel.font = font
        self.titleLabel.textColor = color
    }
    
    func setupSubTitle(text: String?, font: UIFont, color: UIColor){
        self.subTitleLabel.text = text
        self.subTitleLabel.font = font
        self.subTitleLabel.textColor = color
    }
}
