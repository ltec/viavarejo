//
//  AlertView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

protocol AlertViewDelegate: class {
    func didSelected(index: Int)
}

class AlertView: UIAlertController {
    
    static var shared: AlertView?
    
    var flowDelegate: AlertViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loadItems(items: [String]){
        guard let flowDelegate = self.flowDelegate else { return }
        
        for (index, item) in items.enumerated() {
            let action = UIAlertAction(title: item, style: .default, handler: {(item) in
                flowDelegate.didSelected(index: index)
            })
            self.addAction(action)
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        self.addAction(cancelAction)
    }
}
