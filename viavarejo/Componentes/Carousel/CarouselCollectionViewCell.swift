//
//  ProductImageCollectionViewCell.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class CarouselCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
