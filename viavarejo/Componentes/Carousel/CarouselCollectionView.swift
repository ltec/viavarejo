//
//  ProductImagesCollectionView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 14/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

enum CarrocelTypeEnum {
    case Local
    case URL
}

protocol CarouselCollectionViewDelegate {
    func currentPage(page: Int)
}

class CarouselCollectionView: UICollectionView {
    
    lazy var imageService: ImageServiceProtocol = {
        return ImageService()
    }()
    
    var flowDelegate: CarouselCollectionViewDelegate?
    var type: CarrocelTypeEnum = .Local
    let cellIdenfier = "CarouselCollectionViewCell"
    var items: [String] = [] {
        didSet {
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        register(UINib(nibName: cellIdenfier, bundle: nil), forCellWithReuseIdentifier: cellIdenfier)
        self.dataSource = self
        self.delegate = self
        
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: self.frame.width, height: self.frame.height)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        }
    }
    
}

extension CarouselCollectionView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = dequeueReusableCell(withReuseIdentifier: cellIdenfier, for: indexPath) as! CarouselCollectionViewCell
        if type == .Local {
            cell.imageView.image = UIImage(named: self.items[indexPath.section])
        }else {
            self.imageService.downloadImage(from: self.items[indexPath.section], callback: {(image) in
                cell.imageView.image = image
            })
        }
        return cell
    }
}

extension CarouselCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: self.frame.height)
    }
}

extension CarouselCollectionView
: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let flowDelegate = self.flowDelegate else {return}
        flowDelegate.currentPage(page: indexPath.section)
    }
    
    
}
