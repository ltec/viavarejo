//
//  ProductDetailCarouselView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 18/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

class CarouselContentView: UIView {
    
    @IBOutlet weak var collectionView: CarouselCollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var items: [String] = [] {
        didSet {
            self.collectionView.items = self.items
            setupPageControl()
        }
    }
    
    var typeLoadImage: CarrocelTypeEnum = .Local {
        didSet {
            self.collectionView.type = self.typeLoadImage
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        setupPageControl()
        self.collectionView.flowDelegate = self
        self.pageControl.hidesForSinglePage = false
        self.pageControl.currentPage = 0
        self.pageControl.pageIndicatorTintColor = PalleteColor.greyPrimary
        self.pageControl.currentPageIndicatorTintColor = PalleteColor.bluePrimary
        
    }
    
    func setupPageControl(){
        self.pageControl.numberOfPages = self.items.count
    }
}

extension CarouselContentView : CarouselCollectionViewDelegate {
    func currentPage(page: Int) {
        self.pageControl.currentPage = page
    }
}
