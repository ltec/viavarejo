//
//  ECButton.swift
//  HelpFitness
//
//  Created by Leandro Silva Andrade on 07/07/19.
//  Copyright © 2019 Leandro Silva Andrade. All rights reserved.
//

import UIKit
import QuartzCore

public enum ECButtonType {
    case Primary
    case Secondary
    case Clear
}

public enum ECButtonSizeType : CGFloat{
    case Large = 56
    case Medium = 48
    case Small = 30
}

class ButtonView: UIButton {
    
    public var type: ECButtonType = .Primary {
        didSet {
            reframeByColor()
        }
    }
    
    public var size: ECButtonSizeType = .Large {
        didSet {
            reframeBySize()
        }
    }
    
    public var referenceColor = PalleteColor.secondary {
        didSet {
            reframeByColor()
        }
    }
    
    public override var isEnabled: Bool {
        didSet {
            reframeByColor()
        }
    }
    
    /*
    override var isHighlighted: Bool {
        didSet {
            
            if (isHighlighted && self.type == .Primary) {
                self.backgroundColor = UIPalette.yellowPrimaryThree
            }else if(self.type == .Primary) {
                self.backgroundColor = UIPalette.yellowPrimary
            } else {
                self.backgroundColor = self.referenceColor
            }
            
        }
    }*/

    public init(_title:String, _frame:CGRect, _type:ECButtonType, _size:ECButtonSizeType) {
        super.init(frame: _frame)
        self.setTitle(_title, for: .normal)
        self.type = _type
        self.size = _size
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit() {
        layer.cornerRadius = 4
        clipsToBounds = true
        reframeByColor()
        reframeBySize()
    }
    
    public func setButtonTitle(_ title:String) {
        setTitle(title.uppercased(), for: .normal)
    }
    
    private func reframeByColor(){
        switch self.type {
        case .Primary:
            if(self.isEnabled){
                self.backgroundColor = referenceColor
            }else {
                self.backgroundColor = PalleteColor.greySecondary
            }
            self.tintColor = UIColor.white
            self.setTitleColor(UIColor.white, for: .normal)
            break
        case .Secondary:
            if(self.isEnabled){
                self.layer.borderColor = referenceColor.cgColor
                self.tintColor = referenceColor
                self.setTitleColor(referenceColor, for: .normal)
            }else{
                self.layer.borderColor = PalleteColor.greySecondary.cgColor
                self.tintColor = PalleteColor.greySecondary
                self.setTitleColor(PalleteColor.greySecondary, for: .normal)
            }
            self.layer.borderWidth = 1
            self.backgroundColor = UIColor.clear
            break
         case .Clear:
            if(self.isEnabled){
                self.tintColor = referenceColor
                self.setTitleColor(referenceColor, for: .normal)
            }else{
                self.tintColor = PalleteColor.greySecondary
                self.setTitleColor(PalleteColor.greySecondary, for: .normal)
            }
            self.backgroundColor = UIColor.clear
            break
        }
    }
    
    private func reframeBySize(){
        let heightSize: CGFloat;
        switch self.size {
        case .Large:
            self.layer.frame.size.height = CGFloat(ECButtonSizeType.Large.rawValue)
            heightSize = ECButtonSizeType.Large.rawValue
            titleLabel?.font = UIFont.headlineFour()
            break
        case .Medium:
            self.layer.frame.size.height = CGFloat(ECButtonSizeType.Medium.rawValue)
            heightSize = ECButtonSizeType.Medium.rawValue
            titleLabel?.font = UIFont.bodyBold()
            break
        case .Small:
            self.layer.frame.size.height = CGFloat(ECButtonSizeType.Small.rawValue)
            heightSize = ECButtonSizeType.Small.rawValue
            titleLabel?.font = UIFont.overlineBold()
            break
        }
        
        let heightContraints = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: heightSize)
        NSLayoutConstraint.activate([heightContraints])
    }

}
