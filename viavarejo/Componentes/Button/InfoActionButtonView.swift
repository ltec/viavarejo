//
//  InfoActionButtonView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 15/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

@IBDesignable class InfoActionButtonView : UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var actionButton: ButtonView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
    }
    
    func setupTitleLabel(text: String, font: UIFont, color: UIColor, isHidden: Bool){
        titleLabel.text = text
        titleLabel.setup(font: font, color: color)
        titleLabel.isHidden = isHidden
    }
    
    func setupActionButton(text: String, color: UIColor, type: ECButtonType, size: ECButtonSizeType){
        actionButton.setButtonTitle(text)
        actionButton.referenceColor = color
        actionButton.type = type
        actionButton.size = size
    }
    
    func configContentView(backgroundColor: UIColor){
        contentView.backgroundColor = backgroundColor
    }
    
}
