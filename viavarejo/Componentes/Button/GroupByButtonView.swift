//
//  SortButtonView.swift
//  viaverejo
//
//  Created by Ti Corporativo on 12/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit

enum GroupByEnum {
    case List
    case Column
}

protocol GroupByButtonViewDelegate {
    func groupBy(type: GroupByEnum)
}

@IBDesignable class GroupByButtonView : UIView {
    
    var type: GroupByEnum = .Column {
        didSet {
            refreshState()
        }
    }
    var flowDelegate: GroupByButtonViewDelegate?
    @IBOutlet weak var sortListButton: UIButton!
    
    @IBOutlet weak var sortColumnButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        loadViewFromNib()
        
        setupStyle()
    }
    
    func setupStyle(){
        let sortListImage = UIImage(named: "sort_list")?.withRenderingMode(.alwaysTemplate)
        let sortColumnImage = UIImage(named: "sort_column")?.withRenderingMode(.alwaysTemplate)
        
        self.sortListButton.setImage(sortListImage, for: .normal)
        self.sortColumnButton.setImage(sortColumnImage, for: .normal)
        
        self.sortListButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        self.sortColumnButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        refreshState()
    }
    
    func refreshState(){
        if self.type == .List {
            self.sortListButton.tintColor = PalleteColor.bluePrimary
            self.sortColumnButton.tintColor = PalleteColor.greyPrimary
            self.sortListButton.isUserInteractionEnabled = false
            self.sortColumnButton.isUserInteractionEnabled = true
            if let flowDelegate = self.flowDelegate {
                flowDelegate.groupBy(type: self.type)
            }
        } else {
            self.sortListButton.tintColor = PalleteColor.greyPrimary
            self.sortColumnButton.tintColor = PalleteColor.bluePrimary
            self.sortListButton.isUserInteractionEnabled = true
            self.sortColumnButton.isUserInteractionEnabled = false
            if let flowDelegate = self.flowDelegate {
                flowDelegate.groupBy(type: self.type)
            }
        }
    }
    
    @IBAction func sortListButton_didSelected(_ sender: Any) {
        self.type = .List
    }
    
    
    @IBAction func sortColumnButton_didSelected(_ sender: Any) {
        self.type = .Column
    }
    
}
