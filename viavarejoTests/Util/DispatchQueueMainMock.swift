//
//  DispatchQueueMainMock.swift
//  viavarejoTests
//
//  Created by Ti Corporativo on 22/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import UIKit
@testable import viaverejo

class DispatchQueueMainMock: DispatchQueueMainProtocol {
    func async(callback: @escaping (() -> Void)) {
        callback()
    }
}
