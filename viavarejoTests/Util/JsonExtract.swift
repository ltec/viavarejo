//
//  JsonExtract.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//


import Foundation

class JsonExtract {
    func toData(jsonFileName:String) -> Data {
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: jsonFileName, ofType: "json") else { return Data() }
        let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
        return data!
    }
    
    static func extract<T: Codable>(filename: String, callback: @escaping (T?, Error?) -> Void) {
        let objJson:JsonExtract! = JsonExtract()
        let data = try? JSONDecoder().decode(T.self, from: objJson.toData(jsonFileName: filename))
        guard let result = data else {
            callback(nil, ErrorMock())
            return
        }
        callback(result, nil)
    }
    
    
}
