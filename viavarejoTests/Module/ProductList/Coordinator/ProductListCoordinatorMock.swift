//
//  ProductListCoordinatorMock.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//


import UIKit
@testable import viaverejo

class ProductListCoordinatorMock: Coordinator {
    var parentCoordinator: Coordinator?
    var navigationController: UINavigationController?
    var productViewController: ProductListViewController!
    
    init(viewController: ProductListViewController) {
        viewController.flowDelegate = self
        self.productViewController  = viewController
        self.navigationController = UINavigationController()
    }
    
    var childrenCoordinator: [String : Coordinator]?
    
    func start() {
        navigationController?.pushViewController(self.productViewController, animated: true)
    }
}

extension ProductListCoordinatorMock : ProductListViewControllerDelegate {
    func didSelectItemAt() {
        self.navigationController?.pushViewController(ProductDetailViewController(), animated: true)
    }
}
