//
//  ProductListViewTests.swift
//  viaverejoUITests
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import XCTest
import FBSnapshotTestCase
@testable import viaverejo

class ProductListViewTests: FBSnapshotTestCase {

    var viewController: ProductListViewController!
    
    override func setUp() {
        super.setUp()
        let viewController = ProductListViewController(
            productService: ProductListServiceMock(),
            alertService: AlertService()
        )
        
        DispatchQueueMain.main = DispatchQueueMainMock()
        
        _ = viewController.view
        self.viewController = viewController
        
        recordMode = false
    }
    
    func test_viewColumns(){
        viewController.productCollectionView.imageService = ImageServiceMock()
        FBSnapshotVerifyViewController(self.viewController)
    }
    
    func test_viewList(){
        viewController.productCollectionView.imageService = ImageServiceMock()
        self.viewController.groupBy(type: .List)
        FBSnapshotVerifyViewController(self.viewController)
    }
    
    func test_stateFullViewLoading(){
        let view = StateFullView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        view.state = .loading("Loading...")
        FBSnapshotVerifyView(view)
    }
    
    func test_stateFullViewError(){
        let view = StateFullView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        view.state = .error("Error message.")
        FBSnapshotVerifyView(view)
    }
    
    func test_stateFullViewEmpty(){
        let view = StateFullView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        view.state = .empty("Empty message.")
        FBSnapshotVerifyView(view)
    }


}
