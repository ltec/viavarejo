//
//  ProductListCoordinatorTests.swift
//  viavarejoTests
//
//  Created by Ti Corporativo on 17/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import XCTest
@testable import viaverejo

class ProductListCoordinatorTests: XCTestCase {
    
    var coordinator: ProductListCoordinator!
    var navigationController: UINavigationController!

    override func setUp() {
        self.navigationController = UINavigationController()
        self.coordinator = ProductListCoordinator(navigationController:self.navigationController, tabBarItem: UITabBarItem())
        self.coordinator.start()
    }
    
    func test_productDetail(){
        
        if let viewController = self.coordinator.viewController as? ProductListViewController {
            viewController.didSelectItemAt()
        }
        
        XCTAssertTrue(self.navigationController.topViewController is ProductDetailViewController)
    }
    
    func test_getFilterViewController(){
        
        if let viewController = self.coordinator.viewController as? ProductListViewController {
            _ = viewController.view
            viewController.productCollectionView.filter = ProductListFilterModel(evaluation: 1, minPrice: nil, maxPrice: nil)
            self.coordinator.getFilterViewController()
        }
        
        XCTAssertTrue(self.navigationController.topViewController is ProductListFilterViewController)
    XCTAssertNotNil(self.coordinator.getChildrenCoordinator(ProductListFilterCoordinator.self))
    }
    
    func test_applyFilter(){
        
        guard let viewController = self.coordinator.viewController as? ProductListViewController else {return}
        
        _ = viewController.view
        let filter = ProductListFilterModel(evaluation: 5, minPrice: nil, maxPrice: nil)
        self.coordinator.applyFilter(filter: filter)

        XCTAssertEqual(viewController.productCollectionView.filter?.evaluation, 5)
    }

}
