//
//  ProductListViewControllerTests.swift
//  viaverejoTests
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import XCTest
@testable import viaverejo

class ProductListViewControllerTests: XCTestCase {

    
    var viewController: ProductListViewController!
    var productListCoordinator: ProductListCoordinatorMock!
    

    override func setUp() {
        let viewController = ProductListViewController(
            productService: ProductListServiceMock(),
            alertService: AlertService()
        )
        
        self.productListCoordinator = ProductListCoordinatorMock(viewController: viewController)
        self.productListCoordinator.start()

        self.viewController = viewController
        _ = self.viewController.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_instanceIsNotNil(){
        XCTAssertNotNil(self.viewController)
    }
    
    func test_getProducts(){
        self.viewController.getProducts()
        
        XCTAssertEqual(self.viewController.productCollectionView.items.count, 20, "Número de registros não condiz.")
    }
    
    func test_sortProductsForEvaluation(){
        self.viewController.getProducts()
        self.viewController.sortItems(index: SortProductEnum.TopRated.rawValue)
        
        XCTAssertEqual(self.viewController.productCollectionView.items[0].id, 2042243, "Item não condiz.")
    }
    
    func test_sortProductsForMaxPrice(){
        self.viewController.getProducts()
        self.viewController.sortItems(index: SortProductEnum.MaxPrice.rawValue)
        
        XCTAssertEqual(self.viewController.productCollectionView.items[0].id, 9820427, "Item não condiz.")
    }
    
    func test_sortProductsForMinPrice(){
        self.viewController.getProducts()
        self.viewController.sortItems(index: SortProductEnum.MinPrice.rawValue)
        
        XCTAssertEqual(self.viewController.productCollectionView.items[0].id, 8949191, "Item não condiz.")
    }

    func test_showAlertSort() {
        self.viewController.showAlertSort()
    }
    
    func test_didSelectItemAt(){
        self.viewController.didSelectItemAt()
        
       wait(duration: 0.3) { XCTAssertTrue(self.productListCoordinator.navigationController?.topViewController is ProductDetailViewController)
        }
    }
    
    func test_alertDidSelected(){
        self.viewController.didSelected(index: SortProductEnum.TopRated.rawValue)
    
        XCTAssertNil(AlertView.shared)
    }
    
    func test_groupBy(){
        self.viewController.getProducts()
        self.viewController.groupBy(type: .List)
        
        let item = self.viewController.productCollectionView.collectionView(self.viewController.productCollectionView, cellForItemAt: IndexPath(row: 0, section: 1)) as! ProductCollectionViewCell
            
        XCTAssertEqual(self.viewController.productCollectionView.groupBy, GroupByEnum.List, "Formatação da lista nào condiz.")
        XCTAssertEqual(item.groupBy, GroupByEnum.List, "Formatação da célula nào condiz.")
    }

}
