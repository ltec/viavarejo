//
//  ProductListServiceMock.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//
import Foundation
@testable import viaverejo

class ProductListServiceMock: ProductListServiceProtocol {
    
    func getProduct(callback: @escaping (ProductDTO?, Error?) -> Void) {
        JsonExtract.extract(filename: "Products", callback: callback)
    }
}
