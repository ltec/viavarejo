//
//  ImageServiceMock.swift
//  viavarejoTests
//
//  Created by Ti Corporativo on 22/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//
@testable import viaverejo
import UIKit

class ImageServiceMock : ImageServiceProtocol {
    func downloadImage(from url: String, callback: @escaping (UIImage?) -> Void) {
        let image = UIImage(named: url)
        callback(image)
    }
}
