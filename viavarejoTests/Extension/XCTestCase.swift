//
//  XCTest.swift
//  viaverejo
//
//  Created by Ti Corporativo on 16/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    
    func wait(duration: TimeInterval, callback: @escaping () -> Void ) {
        let exp = expectation(description: "wait")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            exp.fulfill()
            callback()
        }
        waitForExpectations(timeout: duration + 1, handler: nil)
    }
}
