# **Teste Via Varejo**

## **Installation**

### **1 - Clone o projeto local e instalando dependências**

<p>1 - Clonando o projeto</p>
<pre><code>git clone https://gitlab.com/ltec/viavarejo.git
</code></pre>

<p>2 - Acessando a pasta do projeto (via terminal)</p>
<pre><code>cd viavarejo
</code></pre>

<p>3 - Instalando as dependências</p>
<pre><code>pod install
</code></pre>

### **2 - Resultado**

###### Splash Screen

![](/markdown-images/splash.png)

###### Home

<img src="/markdown-images/home.png" width="260" height="524" />

###### Lista de produtos (colunas)

![](/markdown-images/products_column.png)

###### Lista de produtos (linhas)

![](/markdown-images/products_list.png)

###### Detalhe do produto

<img src="/markdown-images/product_detail.png" width="260" height="524" />

### **3 - Testes unitários**

<p>Foi utilizado a ferramenta XCTest para testar a parte da lista de produtos (viewcontroller e coordinator)</p>

###### Execução dos testes

![](/markdown-images/unit_test.png)

###### Cobertura de testes no Coordinator (lista de produtos)

![](/markdown-images/coordinator_test.png)

###### Cobertura de testes na viewcontroller (lista de produtos)

![](/markdown-images/viewcontroller_test.png)

### **4 - Testes instrumentados**

###### Testes efetuados na lista de produtos

<img src="/markdown-images/ui_test.gif" width="260" height="524" />

### **5 - Testes Snapshot**

###### 1 - Confirgurar as variáveis de ambiente no Xcode

<table>
    <thead>
        <th>
        Name
        </th>
        <th>
        Value
        </th>
    </thead>
    <tbody>
        <tr>
            <td>
            IMAGE_DIFF_DIR
            </td>
            <td>
            $(SRCROOT)/viavarejoTests/ReferenceImages
            </td>
        </tr>
        <tr>
            <td>
            FB_REFERENCE_IMAGE_DIR
            </td>
            <td>
            $(SRCROOT)/viavarejoTests/FailureDiffs
            </td>
        </tr>
    </tbody>
</table

<p>Acesse a tela de edição do schema</p>

<pre><code>XCode -> Product -> Schema -> Edit Schema..
</code></pre>

<p>Na aba Run crie as variáveis citadas acima</p>

![](/markdown-images/var_snapshot_test.png)

###### 2 - Setar a variável RecordMode

<p>No método setUp do arquivo de teste de snapshot definir o valor da variável Recorde mode como true</p>
                            
![](/markdown-images/config_snapshot_test.png)

<p>Após isso execute o teste e ele ira falhar, não se preocupe pois isso é normal, como o RecordMode está como true, o Xcode está gerando as imagens, verifique se as imagens foram geradas no diretório abaixo:</p>
<pre><code>Raíz -> viavarejoTests -> FailureDiffs_64
</code></pre>

###### 3 - Resultado

<p>Teste de snapshot aplicados na tela listagem de produtos e um componente de estado da view. 
    Qualquer alteração de UI que houver na tela, o teste de snapshot irá quebrar e mostrar as diferenças</p>

![](/markdown-images/result_snapshot_test.png)

### **6 - Acessibilidade**

<p>Acessibilidade aplicada na listagem de produtos, toda vez que o usuário passar o dedo na célula a mensagem retornada será: </p> 

**Nome do produto + avalição + preço anterior + preço atual + forma de parcelamento**

<p>EvaluationView.swift</p>:
<pre><code>    func loadEvaluation(){
        for index in 0...self.numberStars {
            self.stars[index].tintColor =  index < self.evaluation ? PalleteColor.yellowPrimary : PalleteColor.greyPrimary
        }
        self.accessibilityLabel = "Avaliação \(String(self.evaluation)) estrelas"
    }
</code></pre>

<p>PriceView.swift</p>:
<pre><code>    func setupAccessibility(){
        if let oldPrice = self.oldPriceLabel.text, let price = self.priceLabel.text, let payment = self.parcelLabel.text {
            
            self.oldPriceLabel.accessibilityLabel = "De \(oldPrice.replacingOccurrences(of: "R$", with: "")) reais"
            self.priceLabel.accessibilityLabel = "Por \(price.replacingOccurrences(of: "R$", with: "")) reais"
            self.parcelLabel.accessibilityLabel = "Parcelado em \(payment.replacingOccurrences(of: "R$", with: "")) reais"
        }
    }
</code></pre>

<p>ProductCollectionViewCell.swift</p>
<pre><code>    func setupAccessibility(){
        self.isAccessibilityElement = true
        self.accessibilityTraits = UIAccessibilityTraits.selected
        self.accessibilityLabel = self.nameLabel.text
        self.accessibilityHint = "\(evaluationView.accessibilityLabel!) \(priceView.oldPriceLabel.accessibilityLabel!) \(priceView.priceLabel.accessibilityLabel!) \(priceView.parcelLabel.accessibilityLabel!)"
    }
</code></pre>

### **7 - Principais Desafios**

<p>Os principais desafios foram:</p>

- Componentização
- Problemas de configurações do XCode

**Tempo gasto:** 40 horas