//
//  ProductListUITests.swift
//  viavarejoUITests
//
//  Created by Ti Corporativo on 17/12/19.
//  Copyright © 2019 Ti Corporativo. All rights reserved.
//

import XCTest
@testable import viaverejo

class ProductListUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        app = XCUIApplication()
        app.launch()
    }

    func test_smartButtonTabBarTap() {
        app.tabBars.buttons["Smart TV"].tap()
    }
    
    func test_sortAlertTap() {
        app.tabBars.buttons["Smart TV"].tap()
        
        let textField = app/*@START_MENU_TOKEN@*/.otherElements["headerView"]/*[[".otherElements[\"productListView\"].otherElements[\"headerView\"]",".otherElements[\"headerView\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element
        textField.tap()
        
        let ordenarPorAlert = app.alerts["Ordenar por:"]
        ordenarPorAlert.buttons["Menor Preço"].tap()
        textField.tap()
        ordenarPorAlert.buttons["Maior Preço"].tap()
        textField.tap()
        ordenarPorAlert.buttons["Melhor Avaliado"].tap()
        textField.tap()
        ordenarPorAlert.buttons["Cancelar"].tap()
    }
    
    func test_groupItemsTap(){
        app.tabBars.buttons["Smart TV"].tap()
        app.buttons["group_list"].tap()
        app.buttons["group_column"].tap()
    }
    
    func test_navBarTap() {
        app.tabBars.buttons["Smart TV"].tap()
        app.navigationBars["Smart TV"].buttons["filter"].tap()
        app.navigationBars["Filtro"].buttons["Smart TV"].tap()
    }
    
    func test_cellTap(){
        app.tabBars.buttons["Smart TV"].tap()
        app.collectionViews["productCollection"].cells.element(boundBy:0).tap()
    }

}
